% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/methods-allomfit.R
\name{row_to_label}
\alias{row_to_label}
\title{Build labels from the rows and column names of a data frame}
\usage{
row_to_label(x, sep = "|")
}
\arguments{
\item{x}{A data frame.}

\item{sep}{Separator character to use between cell content.}
}
\value{
A character vector containing labels, one label per row of the input
data frame.
}
\description{
Build labels from the rows and column names of a data frame
}
\examples{
x <- tibble::tibble(a = c("apple", "orange", "pear"),
                    b = c("blue", "red", "purple"),
                    c = c(15, 10, 3))
x$label <- row_to_label(x)
x

}
