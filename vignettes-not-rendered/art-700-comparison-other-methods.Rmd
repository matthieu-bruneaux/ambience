---
title: "Comparison of ambience with other methods"
date: "`r Sys.Date()`"
vignette: >
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteIndexEntry{Comparison with other methods}
---

```{r include = FALSE}
knitr::opts_knit$set(verbose = TRUE)
knitr::opts_chunk$set(fig.align = "center")
options(scipen = 6, width = 85)
#options(mc.cores = min(c(2, parallel::detectCores())))
options(mc.cores = parallel::detectCores())
set.seed(4)

# Caching (no simulation is run if the cache file exists)
CACHE_FILE <- "z-cache-700-comparison-other-methods.rds"
CACHED <- file.exists(CACHE_FILE)
```

This document compares the performances of our approach in the `ambience` package with other methods which would either drop negative values or replace them with non-negative values. We use the `etalon` package to keep simulations tidy.

```{r message = FALSE}
library(tidyverse)
library(etalon)
library(ambience)

# For parallel run
library(future)
plan(multicore)
options(mc.cores = parallel::detectCores())
```

## The other methods

There are several ways that allometric data can be analyzed to estimate $\alpha$ and $\beta$. One way can be to linearize the allometric relationship by using a logarithm transformation on $x$ and $y$:
\begin{align}
  \log(y) = \log(\alpha) + \beta \log(x)
\end{align}
and then doing a linear regression on the transformed variables to get $\log(\alpha)$ and $\beta$.

If a log-transform is used, the negative $y$ values can just be dropped, but they cannot be set to zero. An alternative would be to replace the negative $y$ by the smallest non-negative $y$ in the dataset, assuming that this would represent approximatively the detection threshold of the method.

We will call those two methods below `log_drop` and `log_min`.

Another way to approach the problem is to consider the allometric relationship $y=\alpha x^\beta$ and use a non-linear modelling tool, such as a non-linear least squares approach with the `nls()` R function. In this case, `nls()` can be used either on the raw data (keeping the negative values), on data with dropped negative values, or on data where the negative values are set to zero.

We will call those three methods below `nls_raw`, `nls_drop` and `nls_zero`.

## Simulating datasets

Let's first generate the datasets. The first step is to generate some parameter values for the simulations:

```{r }
n_sims <- 500
alpha <- rep(1, n_sims)
beta <- runif(n_sims, min = 0.4, max = 1.2)
sigma_log <- runif(n_sims, min = 0.01, max = 0.2)
sigma_lin <- runif(n_sims, min = 0.01, max = 2)
n_samples <- sample(c(10, 25, 50, 100), size = n_sims, replace = TRUE)
```

Then we combine those parameters into a neat simulation table:

```{r }
z <- new_simtable() %>%
  set_parameters(alpha = alpha,
                 beta = beta,
                 sigma_log = sigma_log,
                 sigma_lin = sigma_lin,
                 .cross = FALSE) %>%
  set_settings(n_samples = n_samples, .cbind = TRUE)
```

We write the function responsible for generating the simulated datasets. It takes as arguments the parameters that we put into the simulation table `z` above:

```{r }
gen_fun <- function(alpha, beta, sigma_log, sigma_lin, n_samples) {
  # Generate "size" trait
  log10_x <- runif(n_samples, min = -1, max = 1)
  x <- 10^log10_x
  # Calculate expected y values
  y_exp <- alpha * x ^ beta
  # Add biological variability (on the log scale)
  log_y_ind <- rnorm(n_samples, mean = log(y_exp), sd = sigma_log)
  y_ind <- exp(log_y_ind)
  # Add measurement variability (on the natural scale)
  y_mes <- rnorm(n_samples, mean = y_ind, sd = sigma_lin)
  return(tibble(x = x, y = y_mes))
}
```

We generate the datasets for all rows of the simulation table:

```{r eval = !CACHED}
z <- generate_datasets(z, gen_fun)
```

We calculate the fraction of negative values in the generated datasets:

```{r }
calc_neg_frac <- function(dataset) {
  return(c(frac_neg = mean(dataset$y < 0)))
}
z <- derive_quantities(z, calc_neg_frac)
```

## Fitting models using the three methods to compare

Now we can write functions to fit an allometry model according to the three methods to compare:

```{r }
# `ambience` method
fit_amb <- function(dataset) {
  m <- allom(y ~ x, data = dataset)
  r <- run_mcmc(m, iter = 2000, chains = 1, control = list(adapt_delta = 0.95))
  stopifnot(all(coda::varnames(r) == c("alpha_(Intercept)", "beta_(Intercept)",
                                  "sigma_log_(Intercept)", "sigma_lin_(Intercept)")))
  coda::varnames(r) <- c("alpha", "beta", "sigma_log", "sigma_lin")
  r
}

# `log_drop` method
fit_log_drop <- function(dataset) {
  d <- dataset[dataset$y > 0, ]
  if (nrow(dataset) < 2) return(NA)
  log_y <- log(d$y)
  log_x <- log(d$x)
  m <- lm(log_y ~ log_x)
  m
}

# `log_min` method
fit_log_min <- function(dataset) {
  min_y <- min(dataset$y[dataset$y > 0])
  if (is.infinite(min_y)) return(NA)
  dataset$y[dataset$y <= 0] <- min_y
  log_y <- log(dataset$y)
  log_x <- log(dataset$x)
  m <- lm(log_y ~ log_x)
  m
}

# `nls_raw` method
fit_nls_raw <- function(dataset) {
  m <- nls(y ~ alpha * x ^ beta, data = dataset,
           start = list(alpha = 1, beta = 1))
  m
}

# `nls_drop` method
fit_nls_drop <- function(dataset) {
  d <- dataset[dataset$y > 0, ]
  if (nrow(d) < 2) return(NA)
  m <- nls(y ~ alpha * x ^ beta, data = d,
           start = list(alpha = 1, beta = 1))
  m
}

# `nls_zero` method
fit_nls_zero <- function(dataset) {
  if (all(dataset$y < 0)) return(NA)
  dataset$y[dataset$y < 0] <- 0
  m <- nls(y ~ alpha * x ^ beta, data = dataset,
           start = list(alpha = 1, beta = 1))
  m
}
```

We fit each model on each dataset. The simulation table `z` is used as input each time and we create one simulation table for each fitting method:

```{r eval = !CACHED, results = "hide", warning = FALSE}
z_amb <- z %>% fit_model(fit_amb)
z_log_methods <- list(z %>% fit_model(fit_log_drop), 
                      z %>% fit_model(fit_log_min))
z_nls_methods <- list(z %>% fit_model(fit_nls_raw),
                      z %>% fit_model(fit_nls_drop),
                      z %>% fit_model(fit_nls_zero))
```

We tidy the results to get the estimates:

```{r }
library(broom)

tidy_log_methods <- function(fit, conf.int = TRUE, conf.level= 0.95) {
  est <- coef(fit)
  ci <- confint(fit, level = conf.level)
  stopifnot(all(names(est) == c("(Intercept)", "log_x")))
  stopifnot(all(rownames(ci) == c("(Intercept)", "log_x")))
  names(est) <- c("alpha", "beta")
  est["alpha"] <- exp(est["alpha"])
  rownames(ci) <- c("alpha", "beta")
  ci["alpha", ] <- exp(ci["alpha", ])
  out <- data.frame(term = c("alpha", "beta"),
                    estimate = est,
                    conf.low = ci[,1],
                    conf.high = ci[,2])
  rownames(out) <- NULL
  out
}
```

We tidy the fits:

```{r eval = !CACHED}
# ambience method
z_amb <- z_amb %>%
  tidy_estimates() %>%
  tidy_intervals(conf.level = seq(0.1, 0.9, by = 0.1))
# log methods
z_log_methods <- lapply(z_log_methods, function(x) {
  x %>% tidy_estimates(tidy_log_methods) %>%
    tidy_intervals(tidy_log_methods, conf.level = seq(0.1, 0.9, by = 0.1))
})
# nls methods
z_nls_methods <- lapply(z_nls_methods, function(x) {
  x %>% tidy_estimates() %>%
    tidy_intervals(conf.level = seq(0.1, 0.9, by = 0.1))
})
```

We put everything together into a single simulation table:
```{r eval = !CACHED}
w <- bind_rows(z_amb, z_log_methods, z_nls_methods)
```

```{r include = FALSE}
if (!CACHED) {
  saveRDS(prune(w), CACHE_FILE)
} else {
  w <- readRDS(CACHE_FILE)
}
```

## Results

Let's visualize the results:

```{r message = FALSE, warning = FALSE}
bias_qplot(w, param = "alpha") + facet_grid(~ method_fit)
bias_qplot(w, param = "beta") + facet_grid(~ method_fit)
```

```{r message = FALSE, warning = FALSE}
intervals_qplot(w, "alpha") + facet_grid(~ method_fit)
intervals_qplot(w, "beta") + facet_grid(~ method_fit)
```

```{r message = FALSE, warning = FALSE}
x <- review(w) %>%
  mutate(frac_neg = cut_interval(true_frac_neg, 5))

ggplot(x, aes(x = true_alpha, y = est_alpha)) +
  geom_boxplot() + facet_grid(setting_n_samples + frac_neg ~ method_fit)

ggplot(x, aes(x = true_beta, y = est_beta)) +
  geom_point() + facet_grid(setting_n_samples ~ frac_neg + method_fit) +
  geom_abline(intercept = 0, slope = 1) + coord_fixed()
```

```{r message = FALSE, warning = FALSE}
i <- summarize_intervals(w, "beta", "n_samples")

ggplot(i, aes(x = prob_level, y = correctness)) +
  geom_point() + geom_line() +
  geom_abline(intercept = 0, slope = 1, col = "green") + 
  facet_grid(n_samples ~ method_fit) + coord_fixed()
```

## Conclusion

## TODOS

TODO: Visualize in relation with the proportion of negative values in the original dataset?

TODO: Add repeated measurements per individual?

TODO: Add a comparison on the effect on population-level estimates?
