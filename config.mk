TOP_DIR=$(shell git rev-parse --show-toplevel)

ORIGINAL_RMD_FILES=$(wildcard vignettes/*.Rmd.original)
COMPILED_RMD_FILES=$(patsubst vignettes/%.Rmd.original, vignettes/%.Rmd, $(ORIGINAL_RMD_FILES))

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
BLUE = "\\033[94m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * help (default rule)

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"
