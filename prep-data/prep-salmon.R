### * Description

# Prepare the dataset from Prokkola et al. 2022:
#
# Prokkola, Jenni M., Eirik R. Åsheim, Sergey Morozov, Paul Bangura, Jaakko
# Erkinaro, Annukka Ruokolainen, Craig R. Primmer, and Tutku Aykanat. ‘Genetic
# Coupling of Life-History and Aerobic Performance in Atlantic
# Salmon’. Proceedings of the Royal Society B: Biological Sciences 289,
# no. 1967 (26 January 2022): 20212500. https://doi.org/10.1098/rspb.2021.2500.

# Data downloaded from https://zenodo.org/record/5667078: "Standard and maximum
# metabolic rate and aerobic scope datasets for Atlantic salmon juveniles."

# Bibtex ref for the zenodo data:
# @dataset{prokkola_jenni_m_2021_5667078,
#   author       = {Prokkola, Jenni M and
#                   Åsheim, Eirik and
#                   Morozov, Sergey and
#                   Bangura, Paul and
#                   Ruokolainen, Annukka and
#                   Primmer, Craig and
#                   Aykanat, Tutku},
#   title        = {{Datasets from Prokkola et al. 2021 bioRxiv, doi: 
#                    https://doi.org/10.1101/2021.08.23.457324).}},
#   month        = nov,
#   year         = 2021,
#   publisher    = {Zenodo},
#   version      = {v.1.0.1, replaced one metadata file for MMR},
#   doi          = {10.5281/zenodo.5667078},
#   url          = {https://doi.org/10.5281/zenodo.5667078}
# }

# md5sum of original files:
# e43b030575642a8c666d1f0e26a35773  All_MMR_batch_info_fixed.txt
# aa5cb426cbd8958c174c93e93bc4672d  Analysed.MMR.data.txt
# bb24503528ada3ba67b034365d701aff  Analysed.SMR.data.txt

### * Setup

suppressPackageStartupMessages({
    library(here)
    library(tidyverse)
    library(lubridate)
})

### * Run

# Load the datasets
spec_smr <- cols(PIT = col_character(), Call_vgll3 = col_character(),
                 Call_six6 = col_character(), Combined = col_character(),
                 Sex = col_character(), Family = col_character(),
                 Batch = col_character(), Chamber.No = col_character(),
                 SMR.mass.mlnd = col_double(), SMR.abs.mlnd = col_double(),
                 SMR.mass.q0.1 = col_double(), SMR.abs.q0.1 = col_double(),
                 SMR.mass.q0.2 = col_double(), SMR.abs.q0.2 = col_double(),
                 Mass = col_double(), Date = col_date(format = ""),
                 Treatment = col_character(), mlndSMR.resid = col_double())
spec_mmr <- cols(Chamber.No = col_character(), Ind = col_character(),
                 Mass = col_double(), Volume = col_double(),
                 Date.Time = col_character(), Phase = col_character(),
                 Temp = col_double(), Slope = col_double(),
                 index = col_double(), DO.unit = col_character(),
                 Batch = col_character(), MR.abs = col_double(),
                 MR.mass = col_double(), Call_six6 = col_character(),
                 Call_vgll3 = col_character(), Sex = col_character(),
                 Family = col_character(), Treatment = col_character())
spec_mmr_extra <- cols(Chamber = col_double(), PIT_MMR = col_character(),
                       Batch = col_character(), weight = col_double(),
                       Date = col_character(), Fam = col_character(),
                       Quadrant = col_character(), Channel = col_double(),
                       initial = col_character(), Order = col_double())
smr <- read_tsv(file.path("data-from-prokkola-2022", "Analysed.SMR.data.txt"),
                col_types = spec_smr)
mmr <- read_tsv(file.path("data-from-prokkola-2022", "Analysed.MMR.data.txt"),
                col_types = spec_mmr)
mmr_extra <- read_tsv(file.path("data-from-prokkola-2022", "All_MMR_batch_info_fixed.txt"),
                      col_types = spec_mmr_extra)

# Select columns
smr <- select(smr,
              PIT,
              vgll3 = Call_vgll3,
              six6 = Call_six6,
              sex = Sex,
              family = Family,
              batch = Batch,
              chamber = Chamber.No,
              mass = Mass,
              smr = SMR.abs.mlnd,
              date = Date,
              food = Treatment)
mmr <- select(mmr,
              PIT = Ind,
              vgll3 = Call_vgll3,
              six6 = Call_six6,
              sex = Sex,
              family = Family,
              batch = Batch,
              chamber = Chamber.No,
              mass = Mass,
              mmr = MR.abs,
              date = Date.Time,
              food = Treatment,
              temp = Temp)
mmr_extra <- mmr_extra %>%
    select(PIT = PIT_MMR, expmr = initial,
           order = Order)
mmr <- left_join(mmr, mmr_extra, by = "PIT")

# Consistency check
z <- full_join(smr, mmr, by = "PIT", suffix = c(".smr", ".mmr"))
for (v in c("vgll3", "six6", "sex", "family", "treatment")) {
    x <- z[[paste0(v, ".smr")]]
    y <- z[[paste0(v, ".mmr")]]
    stopifnot(all(x == y, na.rm = TRUE))
}

# Recode food treatment
mapping <- c("Low food" = "low", "High food" = "high")
smr[["food"]] <- mapping[smr[["food"]]]
mmr[["food"]] <- mapping[mmr[["food"]]]

# Save data
salmon_smr <- smr
salmon_mmr <- mmr
save(salmon_smr, file = here::here("data", "salmon_smr.rda"))
save(salmon_mmr, file = here::here("data", "salmon_mmr.rda"))
