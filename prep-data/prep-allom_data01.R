### * Description

# Simulate allometric data

### * Setup

suppressPackageStartupMessages({
    library(here)
    library(tidyverse)
})

set.seed(45)

### * Simulated dataset "allom_data01"

n_species <- 9
n_locs <- 2
n_days <- 20
sigma_days <- 0.07
days <- paste0("day_", letters[seq_len(n_days)])
a_days <- setNames(rnorm(n_days, 0, sigma_days), days)
lambda <- 80
lambda_per_species <- rpois(n_species, lambda = lambda)
species <- paste0("sp_", letters[seq_len(n_species)])
locs <- paste0("loc_", letters[seq_len(n_locs)])
x_low_species <- setNames(runif(n_species, 0, 0.3), nm = species)
x_high_species <- x_low_species + runif(n_species, 0.2, 0.7)
b_species <- setNames(runif(n_species, 0.6, 1.3), nm = species)
a_species <- setNames(rnorm(n_species, 0, 0.2), nm = species)
a_locs <- setNames(runif(n_locs, 0.1, 0.3), nm = locs)
n_per_species <- rpois(n_species, lambda = lambda_per_species)
N <- sum(n_per_species)
n_batches <- ceiling(N / 20)
batches <- paste0("batch_", seq_len(n_batches))
sigma_batches <- 0.3
ylat_batches <- setNames(rnorm(n_batches, 0, sigma_batches), nm = batches)
batch_plan <- rep(batches, 20)
stopifnot(length(batch_plan) >= N)
slog_species <- setNames(runif(n_species, 0.02, 0.1), nm = species)
min_temp <- 12
max_temp <- 26
mean_temp <- (max_temp + min_temp) / 2
a_temp <- 0.02
slin <- 0.5

true <- list(a_species = a_species, b_species = b_species, a_locs = a_locs,
             a_temp = a_temp,
             sigma_a_days = sigma_days, a_days = a_days, 
             sigma_batches = sigma_batches, batches = ylat_batches,
             slog_species = slog_species,
             slin = slin)

w <- tibble(
    species = rep(species, n_per_species),
    x = 10^runif(N, rep(x_low_species, n_per_species),
                 rep(x_high_species, n_per_species)),
    temp = sample(min_temp:max_temp, N, replace = TRUE),
    loc = sample(locs, N, replace = TRUE),
    day = sample(days, N, replace = TRUE),
    batch = sample(batch_plan[1:N]),
    a = exp(a_species[species]) * exp((temp - mean_temp) * a_temp) *
        exp(a_locs[loc]) * exp(a_days[day]),
    b = b_species[species],
    slog = slog_species[species],
    ypred = a*x^b,
    log_ylat = rnorm(N, mean = log(ypred), sd = slog),
    ylat = exp(log_ylat),
    y = rnorm(N, mean = ylat, sd = slin) + ylat_batches[batch]
)

z <- w %>%
    select(x, y, species, loc, temp, day, batch)
attr(z, "true") <- true

## ggplot(z, aes(x = x, y = y, color = temp)) +
##     geom_point() +
##     facet_wrap(~ species)

## m <- allom(y ~ x + (1 | batch), data = z,
##            alpha ~ species + temp,
##            beta ~ species)

## r <- run_mcmc(m, chains = 2, cores = 2)

## traceplot(r)

## TODO: Document that a "true" values are given for mean temperature. Change
## the simulation so that it is given for e.g. 15°C? In any case the doc for
## allom_data01 should show how to model the data, and maybe a vignette showing
## how centering or shifting a continuous variable is useful for easier
## interpretation of a coefficient would be good.

## Save the data
allom_data01 <- z
save(allom_data01, file = file.path(here::here(), "data", "allom_data01.rda"),
     compress = "xz")

