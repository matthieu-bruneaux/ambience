### * All methods in this file are exported.

# TODO: Add a prior_predict function

### * traceplot() (generic and methods)

#' Generic (S3) for traceplot()
#'
#' @param object An object susceptible to have a trace (e.g. a \code{allomfit}
#'     object).
#' @param ... Passed to the corresponding method.
#' 
#' @export
#'
traceplot <- function(object, ...) {
    UseMethod("traceplot")
}

#' Draw traceplots for an \code{allomfit} object
#'
#' @param object An \code{allomfit} object.
#' @param fit_varnames Boolean, if \code{TRUE} the function will try to fit
#'     parameter names that are too long for display.
#' @param ... Passed to the plotting function.
#'
#' @return This function is called for its side effect of plotting traces of an
#'     \code{allomfit} object.
#'
#' @examples
#' traceplot(gryphons_run)
#' 
#' @method traceplot allomfit
#' @export

traceplot.allomfit <- function(object, fit_varnames = TRUE, ...) {
    plotTraces(object, fit_varnames = fit_varnames, ...)
}

#' Traceplot method for \code{mcmc.list} objects
#'
#' @param object An \code{mcmc.list} object.
#' @param fit_varnames Boolean, if \code{TRUE} the function will try to fit
#'     parameter names that are too long for display.
#' @param ... Passed to the plotting function.
#'
#' @return This function is called for its side effect of plotting traces of an
#'     \code{mcmc.list} object.
#'
#' @method traceplot mcmc.list
#' @export

traceplot.mcmc.list <- function(object, fit_varnames = TRUE, ...) {
    plotTraces(object, fit_varnames = fit_varnames, ...)
}

#' Traceplot method for \code{mcmc} objects
#'
#' @param object An \code{mcmc} object.
#' @param ... Passed to the plotting function.
#'
#' @return This function is called for its side effect of plotting traces of an
#'     \code{mcmc} object.
#'
#' @method traceplot mcmc
#' @export

traceplot.mcmc <- function(object, ...) {
    x <- coda::as.mcmc.list(list(object))
    traceplot(x, ...)
}

### * plot.allomfit()

#' Plot method for \code{allomfit} objects
#'
#' For now this method just passes its argument to \code{traceplot}.
#' 
#' @param x An \code{allomfit} object.
#' @param y Ignored for now.
#' @param ... Passed to the plotting function.
#'
#' @return This function is called for its side effect of plotting an
#'     \code{allomfit} object.
#'
#' @examples
#' plot(gryphons_run)
#' 
#' @method plot allomfit
#' @export

plot.allomfit <- function(x, y, ...) {
    traceplot(x, ...)
}

### * print.allomfit()

#' Print an \code{allomfit} object
#'
#' @param x An \code{allomfit} object.
#' @param ... Passed to the printing method of the stanfit object contained in
#'     \code{x}.
#'
#' @return This function is called for its side effect of printing an
#'     \code{allomfit} object.
#'
#' @examples
#' print(gryphons_run)
#' 
#' @method print allomfit
#' @export

print.allomfit <- function(x, ...) {
    print(summary(x), ...)
}

### * coef.allomfit()

#' Extract coefficients from an \code{allomfit} object
#'
#' @param object An \code{allomfit} object.
#' @param pattern An optional regexpr pattern used to match names of the
#'     parameters from \code{object}.
#' @param FUN Function used to parse the posterior of the parameters to
#'     return. Default to \code{mean} (i.e. returned coefficients are the mean
#'     values of the parameter posteriors).
#' @param ... Additional arguments passed to \code{grepl} when matching
#'     \code{pattern} with the variable names.
#'
#' @return Estimated coefficients for parameters of \code{object}. See the
#'     Examples section for more details.
#'
#' @examples
#' # Default is to return the mean values
#' coef(gryphons_run)
#' # Other functions can be used
#' coef(gryphons_run, FUN = median)
#' # Intervals can also be returned
#' coef(gryphons_run, FUN = ci(0.95))
#'
#' # Parameter names can be matched using regexpr
#' coef(gryphons_run, "^a")
#' coef(gryphons_run, "male")
#' coef(gryphons_run, "^b", FUN = ci(0.89))
#' 
#' @method coef allomfit
#' @export

coef.allomfit <- function(object, pattern = NULL, FUN = mean, ...) {
    vars <- coda::varnames(object)
    if (!is.null(pattern)) {
        vars <- vars[grepl(pattern, vars, ...)]
    }
    object <- object[, vars]
    apply(as.matrix(do.call(rbind, object)), 2, FUN)
}

### * posterior_predict.allomfit()

#' Generate posterior predictions from an \code{allomfit} object
#'
#' TODO: Check that the parameters are as consistent as possible with the
#' methods from rstanarm and brms.
#'
#' TODO: carry over the contrasts.arg param in model.matrix()
#' (cf. parse_fixed_effects()).
#'
#' TODO: Allow for posterior prediction of the parameters a, b, s_log and s_lin
#'
#' TODO: Add function to convert the output from posterior_predict to an
#' mcmc.list object and to add names to it (useful when using `new_data`).
#' 
#' @section About \code{s_log} and \code{s_lin} arguments:
#'
#' If you are calling \code{posterior_predict.allomfit} to perform a posterior
#' predictive check (i.e. with \code{newdata = NULL}), it is recommended to use
#' \code{s_log = TRUE} (and \code{s_lin = TRUE} if the model used \code{s_lin})
#' so that the posterior outcomes are generated in a way that matches the
#' generative model assumed for the observed data.
#'
#' On the other hand, if you are calling \code{posterior_predict.allomfit} to
#' predict posterior outcomes for a new data grid (for example for calculation
#' of marginal effects), it might be best to avoid incorporating the effects of
#' \code{s_log} (and \code{s_lin}) in your calculations.
#'
#' Ultimately, the correct setting of \code{s_log} and \code{s_lin} depends on
#' the user's aim when calling \code{posterior_predict.allomfit}. Running this
#' function without understanding the effect of incorporating \code{s_log} and
#' \code{s_lin} in the predictions can lead to erroneous interpretation of the
#' results.
#'
#' The default function behaviour, when no \code{newdata} is provided, is to
#' assume that the aim is to perform a posterior predictive check and to
#' automatically use \code{s_log = TRUE} (and \code{s_lin = TRUE} if the model
#' used \code{s_lin}).
#'
#' TODO: Incorporate predictions for random effects, add same type of warning
#' as for s_log and s_lin.
#' 
#' @param object An object of class \code{allomfit}.
#' @param newdata An optional data frame for which to predict outcomes. If
#'   \code{NULL} (the default), the original data is used.
#' @param draws Either a positive integer indicating the number of draws to
#'   use, or a vector of integers indicating which draws to use. The default is
#'   to use all draws.
#' @param s_log Boolean. If \code{TRUE}, then posterior prediction incorporates
#'   the variability on the log scale due to sigma_log. If no \code{newdata} is
#'   provided, the default is to use \code{s_log = TRUE}.
#' @param s_lin Boolean. If \code{TRUE} (and if the original model included
#'   \code{s_lin}), then predictions include \code{s_lin} effect. If no
#'   \code{newdata} is provided, the default is to use \code{s_lin = TRUE} if
#'   the model used \code{s_lin}.
#' @param param Optional, one of "a", "b", "s_log", "s_lin", "nu_log", or
#'   "nu_lin". If provided, then the returned matrix contains posterior
#'   predictions for the corresponding allometric parameter instead of for the
#'   response variable. Arguments \code{s_log} and \code{s_lin} are ignored in
#'   this case.
#' @param mcmc Optional, if FALSE (the default), return output in a matrix
#'   format; if TRUE, return output as a named mcmc.list object.
#' @param ... Not used for now.
#'
#' @return A matrix of dimensions n_draws by n_obs containing the predicted
#'     outcomes.
#'
#' @examples
#' # Posterior predictive check
#' # (automatically assumes `s_log = TRUE` and, if applicable, `s_lin = TRUE`)
#' y_rep <- posterior_predict(gryphons_run)
#' if (requireNamespace("bayesplot")) {
#'   bayesplot::ppc_dens_overlay(gryphons$excr, y_rep[1:100, ])
#' }
#'
#' # Predicting outcome for new data grid
#' newdata <- tidyr::crossing(mass = 7, sex = c("male", "female"),
#'                            site = c("memphis", "persepolis"))
#' # When `newdata` is specified, `s_log` and `s_lin` must be explicitly set
#' pp <- posterior_predict(gryphons_run, newdata, s_log = FALSE, s_lin = FALSE)
#' pp <- matrix_to_mcmc(pp, row_to_label(newdata))
#' traceplot(pp)
#' 
#' @method posterior_predict allomfit
#' @importFrom stats rt
#' 
#' @export

posterior_predict.allomfit <- function(object, newdata = NULL, draws = NULL,
                                       s_log = NULL, s_lin = NULL,
                                       param = NULL, mcmc = FALSE, ...) {
  if (!is.null(param)) {
    return(pp_allom_param(object = object, newdata = newdata, draws = draws,
                          param = param, mcmc = mcmc, ...))
  }
  # Extract model
  m <- attr(object, "model")
  x_var <- get_formula_rhv(m$formulas$fixed$response)
  # Parse s_log argument
  if (is.null(s_log)) {
    if (is.null(newdata)) {
      s_log <- TRUE
    } else {
      stop("You must choose `s_log = TRUE` or `s_log = FALSE` for predictions.\n",
           "- If you are performing a posterior predictive check, you probably want `s_log = TRUE`.\n",
           "- If you are generating new data for e.g. marginalization, you might prefer `s_log = FALSE`.\n",
           "(See `?posterior_predict.allomfit` for more details.)")
    }
  }
  # Parse s_lin argument
  if (is.null(s_lin) & !uses_sigma_lin(m)) {
    s_lin <- FALSE
  }
  if (is.null(s_lin) & uses_sigma_lin(m)) {
    if (is.null(newdata)) {
      s_lin <- uses_sigma_lin(m)
    } else {
      stop("The model used `s_lin`. You must choose `s_lin = TRUE` or `s_lin = FALSE` for predictions.\n",
           "- If you are performing a posterior predictive check, you probably want `s_lin = TRUE`.\n",
           "- If you are generating new data for e.g. marginalization, you might prefer `s_lin = FALSE`.\n",
           "(See `?posterior_predict.allomfit` for more details.)")
    }
  }
  if (s_lin & !uses_sigma_lin(m)) {
    stop("The original model did not incorporate `s_lin`.")
  }
  if (!s_log & s_lin) {
    message("Using `s_log = FALSE` and `s_lin = TRUE` seems unusual. ",
            "Are you sure you did not mean to use `s_lin = FALSE`?")
  }
  # Parse newdata
  if (is.null(newdata)) newdata <- m$data
  stopifnot(nrow(newdata) > 0)
  # Extract draws
  posterior <- as.matrix(attr(object, "stanfit"))
  if (!is.null(draws)) {
    if (length(draws) == 1) {
      draws <- sample(seq_len(nrow(posterior)), size = draws)
    }
    posterior <- posterior[draws, , drop = FALSE]
  } else {
    draws <- seq_len(nrow(posterior))
  }
  # Build design matrices for newdata
  model_matrices <- list()
  for (p in model_allom_params(m)) {
    fe_formula <- m$fixed_effects[[p]]$formula
    concat_data <- dplyr::bind_rows(m$data, newdata)
    concat_design <- model.matrix(fe_formula, data = concat_data)
    newdesign <- concat_design[(nrow(m$data)+1):nrow(concat_design), , drop = FALSE]
    stopifnot(all(colnames(newdesign) == colnames(m$fixed_effects[[p]]$model_matrix)))
    rownames(newdesign) <- NULL
    model_matrices[[p]] <- newdesign
  }
  # Calculate posteriors for a, b, s_log, and s_lin
  fe_a <- log(posterior[, startsWith(colnames(posterior), "fe_a"), drop = FALSE])
  fe_b <- log(posterior[, startsWith(colnames(posterior), "fe_b"), drop = FALSE])
  fe_s_log <- log(posterior[, startsWith(colnames(posterior), "fe_s_log"), drop = FALSE])
  if (uses_sigma_lin(m)) {
    fe_s_lin <- log(posterior[, startsWith(colnames(posterior), "fe_s_lin"), drop = FALSE])
  }
  posterior_a <- exp(model_matrices[["a"]] %*% t(fe_a)) # matrix nrow(newdata) x n_draws
  posterior_b <- exp(model_matrices[["b"]] %*% t(fe_b))
  posterior_s_log <- exp(model_matrices[["s_log"]] %*% t(fe_s_log))
  if (s_lin & uses_sigma_lin(m)) {
    posterior_s_lin <- exp(model_matrices[["s_lin"]] %*% t(fe_s_lin))
  }
  # Generate outcome
  x <- do.call(cbind, rep(list(unlist(newdata[, x_var])), length(draws)))
  log_y_pred <- log(posterior_a) + posterior_b * log(x)
  ## Add effect of s_log
  log_y_ind <- matrix(NA, ncol = length(draws), nrow = nrow(newdata))
  for (j in seq_along(draws)) {
    valid_i <- which((!is.na(log_y_pred[, j])) & (!is.na(posterior_s_log[, j])))
    if (s_log) {
      if (m[["family_log"]] == "normal") {
        log_y_ind[valid_i, j] <- rnorm(length(valid_i),
                                       mean = log_y_pred[valid_i, j],
                                       sd = posterior_s_log[valid_i, j])
      } else if (m[["family_log"]] == STRING_T_DIST) {
        posterior_tdist_nu <- posterior[, "nu_log[1]"]
        log_y_ind[valid_i, j] <- (rt(length(valid_i), df = posterior_tdist_nu[j]) *
                                  posterior_s_log[valid_i, j]) + log_y_pred[valid_i, j]
      } else {
        stop("Unknown response distribution: \"", m[["family_log"]], "\"")
      }
    } else {
      log_y_ind[valid_i, j] <- log_y_pred[valid_i, j]
    }
  }
  y_ind <- exp(log_y_ind)
  ## Add effect of s_lin
  y <- matrix(NA, ncol = length(draws), nrow = nrow(newdata))
  if (s_lin & uses_sigma_lin(m)) {
    if (m[["family_lin"]] == "normal") {
      for (j in seq_along(draws)) {
        valid_i <- which((!is.na(y_ind[, j])) & (!is.na(posterior_s_lin[, j])))
        y[valid_i, j] <- rnorm(length(valid_i),
                               mean = y_ind[valid_i, j],
                               sd = posterior_s_lin[valid_i, j])
      }
    } else if (m[["family_lin"]] == STRING_T_DIST) {
      posterior_tdist_nu <- posterior[, "nu_lin[1]"]
      for (j in seq_along(draws)) {
        valid_i <- which((!is.na(y_ind[, j])) & (!is.na(posterior_s_lin[, j])))
        y[valid_i, j] <- (rt(length(valid_i), df = posterior_tdist_nu[j]) *
                          posterior_s_lin[valid_i, j]) + y_ind[valid_i, j]
      }
    } else {
      stop("Unknown response distribution: \"", m[["family_lin"]], "\"")
    }
  } else {
    y <- y_ind
  }
  # Return
  y <- t(y)
  colnames(y) <- NULL
  rownames(y) <- NULL
  if (mcmc) {
    o <- matrix_to_mcmc(y, row_to_label(newdata))
    return(o)
  }
  return(y)
}


### * pp_allom_param()

# TODO Improve mcmc list output by matching the input chains to output chains

#' Generate posterior predictions for primary allometric parameters from an \code{allomfit} object
#'
#' @param object An object of class \code{allomfit}.
#' @param newdata An optional data frame for which to predict outcomes. If
#'   \code{NULL} (the default), the original data is used.
#' @param draws Either a positive integer indicating the number of draws to
#'   use, or a vector of integers indicating which draws to use. The default is
#'   to use all draws.
#' @param param One of \code{c("a", "b", "s_lin", "s_log")}.
#' @param mcmc Boolean, if TRUE (the default) returns an \code{mcmc.list}
#'   object, otherwise returns a matrix.
#' @param ... Not used for now.
#'
#' @examples
#' y_rep <- posterior_predict(gryphons_run)
#' newdata <- tidyr::crossing(mass = 7, sex = c("male", "female"),
#'                            site = c("memphis", "persepolis"))
#' ppb <- posterior_predict_param(gryphons_run, newdata, param = "b")
#' ppb <- matrix_to_mcmc(ppb, row_to_label(newdata))
#' traceplot(ppb)
#' ppslog <- posterior_predict_param(gryphons_run, newdata, param = "s_log")
#' ppslog <- matrix_to_mcmc(ppslog, row_to_label(newdata))
#' traceplot(ppslog)
#' if (requireNamespace("bayesplot")) {
#'   bayesplot::mcmc_intervals(ppb)
#'   bayesplot::mcmc_intervals(ppslog)
#' }
#' 
#' @name pp_allom_param
#' @export

pp_allom_param <- function(object, newdata = NULL, draws = NULL, param,
                           mcmc = FALSE, ...) {
    stopifnot(param %in% c("a", "b", "s_lin", "s_log"))
    # Extract model and parse newdata
    m <- attr(object, "model")
    x_var <- get_formula_rhv(m$formulas$fixed$response)
    if (is.null(newdata)) newdata <- m$data
    stopifnot(nrow(newdata) > 0)
    # Extract draws
    posterior <- as.matrix(attr(object, "stanfit"))
    if (!is.null(draws)) {
        if (length(draws) == 1) {
            draws <- sample(seq_len(nrow(posterior)), size = draws)
        }
        posterior <- posterior[draws, , drop = FALSE]
    } else {
        draws <- seq_len(nrow(posterior))
    }
    # Build design matrix
    fe_formula <- m$fixed_effects[[param]]$formula
    concat_data <- dplyr::bind_rows(m$data, newdata)
    concat_design <- model.matrix(fe_formula, data = concat_data)
    newdesign <- concat_design[(nrow(m$data)+1):nrow(concat_design), , drop = FALSE]
    stopifnot(all(colnames(newdesign) == colnames(m$fixed_effects[[param]]$model_matrix)))
    rownames(newdesign) <- NULL
    model_matrix <- newdesign
    # Calculate posterior
    fe <- log(posterior[, startsWith(colnames(posterior), paste0("fe_", param)),
                        drop = FALSE])
    posterior_param <- exp(model_matrix %*% t(fe))
    # Return
    posterior_param <- t(posterior_param)
    colnames(posterior_param) <- NULL
    rownames(posterior_param) <- NULL
    if (mcmc) {
      o <- matrix_to_mcmc(posterior_param, row_to_label(newdata))
      return(o)
    }
    posterior_param
}

#' @rdname pp_allom_param
#' @export

posterior_predict_param <- pp_allom_param

### * matrix_to_mcmc()

#' Convert a matrix to an \code{mcmc} object.
#'
#' @param x A numeric matrix.
#' @param names A character vector of names to apply to the columns of
#'     \code{x}.
#'
#' @return A \code{coda::mcmc} object.
#'
#' @examples
#' newdata <- tidyr::crossing(mass = 7, sex = c("male", "female"),
#'                            site = c("memphis", "persepolis"))
#' ppb <- posterior_predict_param(gryphons_run, newdata, param = "b")
#' ppb <- matrix_to_mcmc(ppb, row_to_label(newdata))
#' traceplot(ppb)
#' 
#' @export

matrix_to_mcmc <- function(x, names) {
    stopifnot(ncol(x) == length(names))
    colnames(x) <- names
    x <- coda::as.mcmc.list(list(coda::as.mcmc(x)))
    x
}

### * row_to_label()

#' Build labels from the rows and column names of a data frame
#'
#' @param x A data frame.
#' @param sep Separator character to use between cell content.
#'
#' @return A character vector containing labels, one label per row of the input
#'     data frame.
#'
#' @examples
#' x <- tibble::tibble(a = c("apple", "orange", "pear"),
#'                     b = c("blue", "red", "purple"),
#'                     c = c(15, 10, 3))
#' x$label <- row_to_label(x)
#' x
#' 
#' @export

row_to_label <- function(x, sep = "|") {
    v <- colnames(x)
    for (i in seq_along(v)) {
        x[[v[i]]] <- paste0(v[i] ,"_", x[[v[i]]])
    }
    labels <- sapply(seq_len(nrow(x)), function(i) {
        paste(unlist(x[i, ]), collapse = sep)
    })
    labels
}

