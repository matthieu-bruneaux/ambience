### * All functions in this file are exported.

### * prior()

#' Specify a prior
#'
#' @param type String describing the prior type (e.g. "normal", "gamma").
#' @param ... Named arguments specifying the prior parameters. See examples for
#'     more details.
#'
#' @examples
#' prior("normal", mean = 5, sd = 10)
#'
#' @export

prior <- function(type, ...) {
    pars <- list(...)
    z <- get_implemented_priors()
    if (!type %in% z$name) {
        stop(paste0("\"", type, "\" is not a type of prior currently implemented.\n"),
             "Implemented types are:\n",
             paste0(paste0("\"", z$name, "\""), collapse = ", "), ".")
    }
    fun <- z$fun[[type]]
    p <- do.call(fun, pars)
    return(p)
}

### * available_priors()

#' List available priors for an \code{allom} model
#'
#' @return A tibble with the type, the building function and a short
#'     description for each implement prior.
#'
#' @examples
#' available_priors()
#'
#' @export

available_priors <- function() {
    msg <- list(
        "Note that all parameters are truncated at 0, so only the positive portion of any prior is considered during MCMC sampling.",
        "This is why the \"normal\" prior is actually a truncated normal, or why the \"hcauchy\" prior is a half-Cauchy.",
        "", 
        "For more details about a prior definition, type `?<R_function>`, for example `?gamma_p`.")
    priors <- get_implemented_priors()
    priors[["fun"]] <- NULL
    colnames(priors)[colnames(priors) == "name"] <- "type"
    colnames(priors)[colnames(priors) == "function"] <- "R_function"
    cat(format(priors), sep = "\n")
    cat("\n")
    lapply(msg, function(x) cat(strwrap(x), sep = "\n"))
    invisible(priors)
}

### * constant_p(value)

#' Define a fixed-value prior
#'
#' This is equivalent to having a fixed parameter.
#'
#' @param value The constant value of the parameter.
#'
#' @return A list defining the prior.
#'
#' @examples
#' constant_p(2)
#'
#' @export

constant_p <- function(value) {
    x <- list(type = "constant",
              parameters = c(value = value))
    x <- structure(x, class = "prior")
    return(x)
}
attr(constant_p, "description") <- "Constant value (not sampled by MCMC)"
attr(constant_p, "fun_name") <- "constant_p"

### * uniform_p(min, max)

#' Define a uniform prior
#'
#' @param min,max Minimum and maximum boundaries for the uniform prior.
#'
#' @return A list defining the prior.
#'
#' @importFrom stats runif
#' 
#' @examples
#' uniform_p(min = 0, max= 1)
#'
#' @export

uniform_p <- function(min, max) {
    x <- list(type = "uniform",
              parameters = c(min = min, max = max))
    x <- structure(x, class = "prior")
    return(x)
}
attr(uniform_p, "description") <- "Uniform between `min` and `max`"
attr(uniform_p, "fun_name") <- "uniform_p"

### * lognormal(mean, sd)

#' Define a log normal prior
#'
#' @param mean Mean of the log-transformed variable.
#' @param sd Standard deviation of the log-transformed variable.
#'
#' @return A list defining the prior.
#'
#' @examples
#' lognormal_p(mean = 0, sd = 0.7)
#'
#' @export

lognormal_p <- function(mean, sd) {
  x <- list(type = "lognormal",
            parameters = c(mean = mean,
                           sd = sd))
  x <- structure(x, class = "prior")
  return(x)
}
attr(lognormal_p, "description") <- "Lognormal with `mean` and `sd` for log-transformed variable"
attr(lognormal_p, "fun_name") <- "lognormal_p"

### * normal_p(mean, sd)

#' Define a truncated normal prior (on \code{[0;+Inf]})
#'
#' \code{normal_p} and \code{tnormal_p} are synonymous.
#' 
#' @param mean Mean of the untruncated normal.
#' @param sd Standard deviation of the untruncated normal.
#'
#' @return A list defining the prior.
#'
#' @importFrom stats rnorm
#'
#' @examples
#' normal_p(mean = 0, sd = 4)
#'
#' @export

normal_p <- function(mean, sd) {
    x <- list(type = "trun_normal",
              parameters = c(mean = mean,
                             sd = sd))
    x <- structure(x, class = "prior")
    return(x)
}
attr(normal_p, "description") <- "Normal with mean `mean` and standard deviation `sd`"
attr(normal_p, "fun_name") <- "normal_p"

#' Aliases for normal_p
#'
#' @rdname normal_p
#' @export
tnormal_p <- normal_p

### * cauchy_p(scale)

#' Define a Cauchy prior (truncated on [0;+Inf), this becomes a half-Cauchy)
#'
#' \code{cauchy_p} and \code{hcauchy_p} are synonymous.
#' 
#' @param scale Median of the half-Cauchy distribution.
#'
#' @return A list defining the prior.
#'
#' @importFrom stats rcauchy
#'
#' @examples
#' cauchy_p(scale = 0.5)
#'
#' @export

cauchy_p <- function(scale) {
    x <- list(type = "hcauchy",
              parameters = c(scale = scale))
    x <- structure(x, class = "prior")
    return(x)
}
attr(cauchy_p, "description") <- "Cauchy with location 0 and scale `scale`"
attr(cauchy_p, "fun_name") <- "cauchy_p"

#' Aliases for cauchy_p
#'
#' @rdname cauchy_p
#' @export
hcauchy_p <- cauchy_p

### * gamma_p(mean, sd, cv, shape, rate)

#' Define a gamma prior by either (mean, cv), (mean, sd) or (shape, rate)
#'
#' @param mean,sd,cv,shape,rate Parameters for the gamma distribution. Only
#'     valid pairs of arguments are (mean, sd), (mean, cv) and (shape, rate).
#'
#' @examples
#' gamma_p(mean = 1, cv = 0.2)
#'
#' @export

gamma_p <- function(mean, sd, cv, shape, rate) {
    if (!missing(mean) & !missing(sd)) {
        stopifnot(missing(cv) & missing(shape) & missing(rate))
        rate <- mean / (sd^2)
        shape <- mean * rate
    } else if (!missing(mean) & !missing(cv)) {
        stopifnot(missing(sd) & missing(shape) & missing(rate))
        sd = mean * cv
        rate <- mean / (sd^2)
        shape <- mean * rate
    } else if (!missing(shape) & !missing(rate)) {
        stopifnot(missing(mean) & missing(sd) & missing(cv))
    } else { stop("Bad pairing of arguments") }
    x <- list(type = "gamma",
              parameters = c(shape = shape, rate = rate))
    x <- structure(x, class = "prior")
    return(x)
}
attr(gamma_p, "description") <- "Gamma defined by (mean, sd), (mean, cv) or (shape, rate)"
attr(gamma_p, "fun_name") <- "gamma_p"
