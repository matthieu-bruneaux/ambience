### * All the functions in this file are exported.

# TODO: Replace ci() by posterior_interval from rstantools?

### * ci()

#' Build a function to calculate credible intervals
#'
#' @param prob Probability for the credible intervals to be calculated.
#'
#' @return A function that calculates quantiles related to the \code{prob}
#'     argument.
#'
#' @examples
#' set.seed(4)
#' x <- rnorm(1e4)
#' q50 <- ci(0.5)
#' q95 <- ci(0.95)
#' q99 <- ci(0.99)
#'
#' q50(x)
#' q95(x)
#' q99(x)
#'
#' @export

ci <- function(prob) {
    stopifnot(prob >= 0 & prob <= 1)
    probs <- c((1 - prob)/2, 1 - (1 - prob)/2)
    force(probs)
    f <- function(x) stats::quantile(x, probs = probs)
    f
}
