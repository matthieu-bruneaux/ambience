### * Description

# Methods for \code{coda::mcmc.list} objects:
#
# - Provide simple math operations for mcmc.list objects and also allows to use
# math functions such as log() or exp() on mcmc.list objects.
#
# - Provide simple interface to select parameters based on their name.
#
# Those methods are not designed specifically for this package, and could be
# submitted for integration in the coda package.

# TODO: Load subsetting methods for mcmc.list from coda?

### * Helper functions

### ** as.derived.mcmc.list()

as.derived.mcmc.list <- function(x) {
    if (!"derived.mcmc.list" %in% class(x)) {
        return(structure(x, class = c("derived.mcmc.list", class(x))))
    } else {
        return(x)
    }
}

### * Ops.mcmc.list()

# Based on
# ?groupGeneric
# ?.Generic
# https://stackoverflow.com/questions/35902360/r-implement-group-generics-ops-to-enable-comparison-of-s3-objects
# (cdeterman answer in the above)

#' Ops generics for \code{\link[coda]{mcmc.list}} objects
#'
#' @param e1 First operand
#' @param e2 Second operand
#'
#' @examples
#' \dontrun{
#' # If a is a coda::mcmc.list object
#' plot(a)
#' plot(a["uptakeRate_from_NH4_to_epi.act"] - a["uptakeRate_from_NO3_to_epi.act"])
#' plot(a["uptakeRate_from_NH4_to_epi.act"] + a["uptakeRate_from_NO3_to_epi.act"])
#' plot(a["uptakeRate_from_NH4_to_epi.act"] * a["uptakeRate_from_NO3_to_epi.act"])
#' plot(a["uptakeRate_from_NH4_to_epi.act"] / a["lossRate_epi.act"])
#' plot(a["uptakeRate_from_NH4_to_epi.act"] - 10)
#' plot(a["uptakeRate_from_NH4_to_epi.act"] + 10)
#' plot(a["uptakeRate_from_NH4_to_epi.act"] * 10)
#' plot(a["uptakeRate_from_NH4_to_epi.act"] / 10)
#' plot(10 - a["uptakeRate_from_NO3_to_epi.act"])
#' plot(10 + a["uptakeRate_from_NO3_to_epi.act"])
#' plot(10 * a["uptakeRate_from_NO3_to_epi.act"])
#' plot(10 / a["lossRate_epi.act"])
#' }
#'
#' @method Ops mcmc.list
#'
#' @export

Ops.mcmc.list = function(e1, e2) {
    # Modified from cdeterman answer from:
    # https://stackoverflow.com/questions/35902360/r-implement-group-generics-ops-to-enable-comparison-of-s3-objects
    op = .Generic[[1]]
    if ("mcmc.list" %in% class(e1) & "mcmc.list" %in% class(e2)) {
        areCompatible = function(m1, m2) {
            COMPATIBLE = TRUE
            if (coda::nvar(m1) != coda::nvar(m2)) {
                COMPATIBLE = FALSE
            } else if (coda::nchain(m1) != coda::nchain(m2)) {
                COMPATIBLE = FALSE
            } else if (!all(coda::varnames(m1) == coda::varnames(m1))) {
                COMPATIBLE = FALSE
            } else if (!all(coda::mcpar(m1[[1]]) == coda::mcpar(m2[[1]]))) {
                COMPATIBLE = FALSE
            }
            return(COMPATIBLE)
        }
        if (!areCompatible(e1, e2)) {
            stop("Incompatible inputs")
        }
        if (coda::nvar(e1) != 1) {
            stop("Ops implemented only for single-parameter chains")
        }
        switch(op,
               "+" = {
                   # Addition
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] + e2[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               "-" = {
                   # Subtraction
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] - e2[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               "*" = {
                   # Multiplication
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] * e2[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               "/" = {
                   # Division
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] / e2[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               stop("Undefined operation for mcmc.list objects"))
    } else if ("mcmc.list" %in% class(e1)) {
        stopifnot("mcmc.list" %in% class(e1) & ! "mcmc.list" %in% class(e2))
        stopifnot(is.numeric(e2) & length(e2) == 1)
        switch(op,
               "+" = {
                   # Addition
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] + e2
                   }
                   return(as.derived.mcmc.list(c))
               },
               "-" = {
                   # Subtraction
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] - e2
                   }
                   return(as.derived.mcmc.list(c))
               },
               "*" = {
                   # Multiplication
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] * e2
                   }
                   return(as.derived.mcmc.list(c))
               },
               "/" = {
                   # Division
                   c = e1
                   for (i in 1:coda::nchain(e1)) {
                       c[[i]] = c[[i]] / e2
                   }
                   return(as.derived.mcmc.list(c))
               },
               stop("Undefined operation for mcmc.list object and numeric")
               )
    } else {
        stopifnot("mcmc.list" %in% class(e2) & ! "mcmc.list" %in% class(e1))
        stopifnot(is.numeric(e1) & length(e1) == 1)
        switch(op,
               "+" = {
                   # Addition
                   c = e2
                   for (i in 1:coda::nchain(e2)) {
                       c[[i]] = c[[i]] + e1
                   }
                   return(as.derived.mcmc.list(c))
               },
               "-" = {
                   # Subtraction
                   c = e2
                   for (i in 1:coda::nchain(e2)) {
                       c[[i]] = e1 - c[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               "*" = {
                   # Multiplication
                   c = e2
                   for (i in 1:coda::nchain(e2)) {
                       c[[i]] = c[[i]] * e1
                   }
                   return(as.derived.mcmc.list(c))
               },
               "/" = {
                   # Division
                   c = e2
                   for (i in 1:coda::nchain(e2)) {
                       c[[i]] = e1 / c[[i]]
                   }
                   return(as.derived.mcmc.list(c))
               },
               stop("Undefined operation for mcmc.list object and numeric")
               )
    }
}

### * Math.mcmc.list()

#' Math generics for mcmc.list objects
#'
#' @param x \code{\link[coda]{mcmc.list}} object
#' @param ... Other arguments passed to corresponding methods
#'
#' @method Math mcmc.list
#'
#' @export

Math.mcmc.list = function(x, ...) {
    out = lapply(x, .Generic, ...)
    out = lapply(out, coda::mcmc)
    out = coda::mcmc.list(out)
    out = as.derived.mcmc.list(out)
    return(out)
}

### * select.mcmc.list()

#' Select parameters based on their names
#'
#' @param .data An \code{mcmc.list} object from which to select parameters.
#' @param ... Patterns used to match parameter names in \code{.data}.
#' @param exact Boolean, if TRUE only parameters with exact matches are
#'   returned, otherwise (the default) matches are determined with
#'   \code{grepl}.
#' 
#' @return An \code{mcmc.list} object, with the same extra class(es) as
#'     \code{.data} (if any).
#'
#' @importFrom dplyr select
#' @importFrom purrr map
#' @importFrom rlang ensyms
#' @importFrom rlang as_string
#' @method select mcmc.list
#' @export
#'
#' @examples
#' gryphons_run %>% select("a[[]")
#' traceplot(gryphons_run %>% select("excr[[]"))

select.mcmc.list <- function(.data, ..., exact = FALSE) {
    params <- coda::varnames(.data)
    patterns <- rlang::ensyms(...)
    patterns <- purrr::map(patterns, rlang::as_string)
    if (exact) {
        matches <- lapply(patterns, function(p) which(params == p))
    } else {
        matches <- lapply(patterns, function(p) which(grepl(p, params, ...)))
    }
    matches <- sort(unique(unlist(matches)))
    if (length(matches) == 0) { return(NULL) }
    out <- .data[, matches]
    out <- lapply(out, function(o) {
        if (is.null(dim(o))) {
            o <- matrix(o, ncol = 1)
        }
        o <- coda::as.mcmc(o)
        colnames(o) <- coda::varnames(.data)[matches]
        return(o)
    })
    class(out) <- class(.data)
    return(out)
}

### * c.mcmc.list (to remove?)

#' Merge mcmc.list objects
#'
#' @param ... \code{mcmc.list} object to concatenate side-by-side (i.e. like a
#'     cbind(), not like an rbind()).
#' @method c mcmc.list
#'
#' @export

c.mcmc.list <- function(...) {
    x <- list(...)
    if (is.null(names(x))) {
        names(x) <- rep("", length(x))
    }
    for (i in seq_along(x)) {
        n_vars <- ncol(as.matrix(x[[i]]))
        if (names(x)[i] == "") {
            if (is.null(coda::varnames(x[[i]]))) {
                coda::varnames(x[[i]]) <- rep("unnamed_variable", n_vars)
            }
        } else {
            coda::varnames(x[[i]]) <- rep(names(x)[i], n_vars)
        }
    }
    out <- x[[1]]
    if (length(x) == 1) {
        return(out)
    }
    for (i in 2:length(x)) {
        y <- x[[i]]
        stopifnot(length(out) == length(y))
        for (j in seq_along(out)) {
            c_out <- out[[j]]
            c_y <- y[[j]]
            stopifnot(all(coda::mcpar(c_out) == coda::mcpar(c_y)))
            c_merged <- cbind(c_out, c_y)
            colnames(c_merged) <- c(coda::varnames(c_out), coda::varnames(c_y))
            attr(c_merged, "mcpar") <- attr(c_out, "mcpar")
            class(c_merged) <- "mcmc"
            out[[j]] <- c_merged
        }
        out <- coda::as.mcmc.list(out)
    }
    return(out)
}
