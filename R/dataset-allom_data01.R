#' A simulated dataset including fixed and random effects
#'
#' This simulated dataset includes categorical and continuous fixed effects and
#' categorical random effects.
#'
#' @details
#' See the Examples section for an allometric model that matches the generative
#' model used to simulate the data.
#'
#' Note that the true parameter values can be obtained with
#' \code{attr(allom_data01, "true")}.
#' 
#' @format A tibble with 719 observations and 7 variables. All observations are
#'     assumed to be made only once (no repeated measures).
#' 
#' \describe{
#' 
#'   \item{x}{The independent "size" trait for the allometric relationship.}
#'
#'   \item{y}{The "response" trait for the allometric relationship.}
#'
#'   \item{species}{A categorical variable with 9 species.}
#'
#'   \item{loc}{A categorical variable with 2 locations.}
#'
#'   \item{temp}{A continuous variable representing temperature in Celsius.}
#'
#'   \item{day}{A categorical variable with 20 days (sample collection
#'   days).}
#'
#'   \item{batch}{A categorical variable with 36 batches (sample analytical
#'   measurement batches).}
#' }
#'
#' @examples
#' # Allometric model matching the true generative model
#' m <- allom(y ~ x + (1 | batch),
#'            a ~ species + temp + loc + (1 | day),
#'            b ~ species,
#'            s_log ~ species,
#'            data = allom_data01)
#' m
#'
#' # True parameter values
#' true <- attr(allom_data01, "true")
#' # Show only 2 significant digits for compact display
#' lapply(true, signif, 2)
#' 
#' @source Simulated dataset.
#'
#' @family {allometry datasets}
"allom_data01"
