#' Standard and maximum metabolic rates in Atlantic salmon juveniles
#'
#' A slightly modified version of the dataset published by
#' \href{https://doi.org/10.5281/zenodo.5667078}{Prokkola et al. (2021)} (see
#' the Source section for the full references).
#'
#' @details
#' This dataset was published on Zenodo
#' (\href{https://doi.org/10.5281/zenodo.5667078}{Prokkola et al. 2021}) and
#' contains the data from the original article
#' "Genetic Coupling of Life-History and Aerobic Performance in Atlantic Salmon"
#' (\href{https://doi.org/10.1098/rspb.2021.2500}{Prokkola et al. 2022}).
#'
#' In the original article, the authors explore how energy metabolism in
#' juvenile Atlantic salmon (\emph{Salmo salar}) is affected by two genomic
#' regions (\emph{vgll3} and \emph{six6}) and food availability, based on
#' individuals raised in common garden conditions.
#'
#' See the Source section for more details about the origin of this dataset.
#'
#' @format \code{salmon_smr} contains standard metabolic rate data. It is a
#'     tibble with 264 observations and 11 variables. It is derived from the
#'     original dataset file \code{"Analysed.SMR.data.txt"} from Prokkola et
#'     al. (2021).
#'
#' \describe{
#'
#'   \item{PIT}{Presumably a PIT tag reference used to uniquely identify
#'   individuals. Column in original dataset: \code{"PIT"}.}
#'
#'   \item{vgll3}{Genotype at the \emph{vgll3} region. Column in original
#'   dataset: \code{"Call_vgll3"}.}
#'
#'   \item{six6}{Genotype at the \emph{six6} region. Column in original
#'   dataset: \code{"Call_six6"}.}
#'
#'   \item{sex}{Individual sex. Column in original dataset: \code{"Sex"}.}
#'
#'   \item{family}{Family of the individual. Column in original dataset:
#'   \code{"Family"}.}
#'
#'   \item{batch}{Batch for SMR measurements. Column in original dataset:
#'   \code{"Batch"}.}
#'
#'   \item{chamber}{Presumably the identity of the chamber used for SMR
#'   measurements. Column in original dataset: \code{"Chamber.no"}.}
#'
#'   \item{mass}{Individual mass. Column in original dataset: \code{"Mass"}.}
#'
#'   \item{smr}{Standard metabolic rate. Column in original dataset:
#'   \code{"SMR.abs.mlnd"}.}
#'
#'   \item{date}{Presumably measurement date. Column in original dataset:
#'   \code{"Date"}.}
#'
#'   \item{food}{Food treatment (low/high food). Column in original dataset:
#'   \code{"Treatment"}. The original values were recoded from "Low food" and
#'   "High food" to "low" and "high", respectively.}
#'
#' }
#' 
#' @source The original dataset was published in:
#'
#' Prokkola, Jenni M, Eirik Åsheim, Sergey Morozov, Paul Bangura, Annukka
#' Ruokolainen, Craig Primmer, and Tutku Aykanat. ‘Datasets from Prokkola et
#' al. 2021 BioRxiv, Doi: Https://Doi.Org/10.1101/2021.08.23.457324).’ Zenodo,
#' 14 November 2021. \url{https://doi.org/10.5281/zenodo.5667078}.
#'
#' The dataset was a companion to the following study:
#'
#' Prokkola, Jenni M., Eirik R. Åsheim, Sergey Morozov, Paul Bangura, Jaakko
#' Erkinaro, Annukka Ruokolainen, Craig R. Primmer, and Tutku Aykanat. ‘Genetic
#' Coupling of Life-History and Aerobic Performance in Atlantic
#' Salmon’. Proceedings of the Royal Society B: Biological Sciences 289,
#' no. 1967 (26 January 2022):
#' 20212500. \url{https://doi.org/10.1098/rspb.2021.2500}.
#'
#' About copyright and license for this dataset: as indicated on the Zenodo
#' page (\url{https://zenodo.org/record/5667078}), the license for the files is
#' Creative Commons Attribution 4.0 International
#' (\href{https://creativecommons.org/licenses/by/4.0/legalcode}{link}).
#'
#' @family {allometry datasets}
#'
#' @name salmon
#'
#' @examples
#' # The effects included in the model below match those in the model used by
#' # Prokkola et al. (2022).
#' m <- allom(smr ~ mass, data = salmon_smr, s_lin = FALSE,
#'            a ~ food + sex + vgll3*six6 +
#'                (food + sex):(vgll3 + six6) +
#'                (1 | family + batch + chamber),
#'            b ~ food)
#'
#' \dontrun{
#' r <- run_mcmc(m)
#' plot(r)
#' }
#' 
"salmon_smr"

#' @format \code{salmon_mmr} contains maximum metabolic rate data. It is a
#'     tibble with 264 observations and 12 variables. It is derived from the
#'     original dataset files \code{"Analysed.MMR.data.txt"} ("file 1" in
#'     column description below) and \code{"All_MMR_batch_info_fixed.txt"}
#'     ("file 2" in column description below) from Prokkola et al. (2021),
#'     which were merged together by matching columns \code{"Ind"} (file 1) and
#'     \code{"PIT"} (file 2).
#'
#' \describe{
#'
#'   \item{PIT}{Presumably a PIT tag reference used to uniquely identify
#'   individuals. Column in original dataset: \code{"Ind"} (file 1).}
#'
#'   \item{vgll3}{Genotype at the \emph{vgll3} region. Column in original
#'   dataset: \code{"Call_vgll3"} (file 1).}
#'
#'   \item{six6}{Genotype at the \emph{six6} region. Column in original
#'   dataset: \code{"Call_six6"} (file 1).}
#'
#'   \item{sex}{Individual sex. Column in original dataset: \code{"Sex"} (file
#'   1).}
#'
#'   \item{family}{Family of the individual. Column in original dataset:
#'   \code{"Family"} (file 1).}
#'
#'   \item{batch}{Batch for MMR measurements. Column in original dataset:
#'   \code{"Batch"} (file 1).}
#'
#'   \item{chamber}{Presumably the identity of the chamber used for MMR
#'   measurements. Column in original dataset: \code{"Chamber.no"} (file 1).}
#'
#'   \item{mass}{Individual mass. Column in original dataset: \code{"Mass"}
#'   (file 1).}
#'
#'   \item{mmr}{Maximum metabolic rate. Column in original dataset:
#'   \code{"MR.abs"} (file 1).}
#'
#'   \item{date}{Presumably measurement date. Column in original dataset:
#'   \code{"Date.Time"} (file 1).}
#'
#'   \item{food}{Food treatment (low/high food). Column in original dataset:
#'   \code{"Treatment"} (file 1). The original values were recoded from
#'   "Low food" and "High food" to "low" and "high", respectively.}
#' 
#'   \item{temp}{Presumably water temperature in the measurement chamber, in
#'   degree Celsius. Column in original dataset: \code{"Temp"} (file 1).}
#'
#'   \item{expmr}{(Short for experimenter) presumably the initial of the person
#'   realizing the chase test to measure MMR. Column in original dataset:
#'   \code{"initial"} (file 2).}
#'
#'   \item{order}{Order in MMR was measured for pairs of fish each day
#'   (values between 1 and 8). Column in original dataset: \code{"Order"} (file
#'   2).}
#' 
#' }
#'
#' @examples
#' # The effects included in the model below match those in the model used by
#' # Prokkola et al. (2022).
#' m <- allom(mmr ~ mass, data = salmon_mmr, s_lin = FALSE,
#'            a ~ food + sex + vgll3*six6 + order +
#'                (food + sex):(vgll3 + six6) +
#'                (1 | family + expmr + chamber),
#'            b ~ food)
#'
#' \dontrun{
#' r <- run_mcmc(m)
#' plot(r)
#' }
#' 
#' @rdname salmon
"salmon_mmr"
