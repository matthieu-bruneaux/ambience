#' Test function for lm with Stan
#'
#' @param x Vector of input values
#' @param y Vector of output values
#' @param ... Arguments passed to \code{rstan::sampling()}
#'
#' @return An object of class stanfit returned by \code{rstan::sampling()}
#' 
#' @export

lm_stan <- function(x, y, ...) {
    stan_data <- list(x = x, y = y, N = length(x))
    out <- rstan::sampling(stanmodels$lm, data = stan_data, ...)
    return(out)
}
