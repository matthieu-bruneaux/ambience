### * All the functions in this file are exported

### * extract_eq()

#' Generic function to extract equations from models or fits
#'
#' @param x Object from which to extract equations.
#' @param ... Other arguments passed to the corresponding method.
#'
#' @return This function is mainly called for its side effect of printing (via
#'   \code{cat}) some LaTeX code that can be directly incorporated into R
#'   markdown documents.
#'
#' @export

extract_eq <- function(x, ...) {
  UseMethod("extract_eq")
}

### * extract_eq.allom()

### ** Doc

# Note for dev: The default maximumm length for a MathJax input string is 5K
# characters
# (https://docs.mathjax.org/en/v3.0-latest/options/input/tex.html). Maybe warn
# the user about possible issues when the string is too long? (For example,
# check the length of the returned string and throw a warning when the length
# is close to maximum.)

#' Extract pretty LaTeX equations from an allometric model
#'
#' Note: This function is in an experimental state and substantial changes
#' might occur in the future (both in the arguments it takes and in its output)!
#'
#' @param x A model built with \code{allom()}.
#' @param ... Arguments passed to downstream formatting functions.
#' @param core Boolean, should the core allometric model be included in the
#'   output?
#' @param allom_params Boolean, should the equations describing fixed and random
#'   effects on the primary allometric parameters be included in the output?
#' @param re_dist Boolean, should the distributions describing the random
#'   effects of grouping factors be included in the output?
#' @param priors Boolean, should the priors be included in the output?
#' @param core_rev Boolean, should the display order of the equations for the
#'   core allometric model be reversed?
#' @param verbosity 0, 1, or 2.
#' @param formatter Optional, a formatter object responsible for the formatting
#'   of various equation elements (matrices, vectors, indexing style, ...).
#' @param print Boolean, should the equation be displayed? Default is
#'   \code{TRUE}.
#'
#' @return This function is mainly called for its side effect of printing (via
#'   \code{cat}) some LaTeX code that can be directly incorporated into R
#'   markdown documents. It also returns invisibly the strings comprising this
#'   LaTeX code.
#'
#' @examples
#' m <- allom(excr ~ mass, data = gryphons)
#' extract_eq(m)
#' extract_eq(m, index = "i")
#'
#' @method extract_eq allom
#' 
#' @export

### ** Code

extract_eq.allom <- function(x, ..., core = TRUE, allom_params = TRUE, re_dist = TRUE,
                             priors = FALSE, core_rev = FALSE, 
                             verbosity = 1, formatter, print = TRUE) {
  formatter <- parse_formatter()
  # Core equation
  if (core) {
    eq_core <- desc_core(x = x, formatter = formatter, core_rev = core_rev,
                         verbosity = verbosity, ...)
  } else {
    eq_core <- NULL
  }
  # Equations for primary allometric parameters
  if (allom_params) {
    eq_params <- desc_allom_param(x = x, formatter = formatter,
                                  verbosity = verbosity, ...)
  } else {
    eq_params <- NULL
  }
  # Equations for random effects coefficients
  if (re_dist) {
    eq_re <- desc_re(x = x, formatter = formatter, verbosity = verbosity, ...)
  } else {
    eq_re <- NULL
  }
  # Priors
  if (priors) {
    eq_priors <- desc_priors(x = x, formatter = formatter,
                             verbosity = verbosity, ...)
  } else {
    eq_priors <- NULL
  }
    # Build output
  eq <- c(eq_core, " ",
          eq_params, " ", 
          eq_re, " ",
          eq_priors)
  # Return (also print if required)
  if (print) {
    cat(eq, sep = "\n")
  }
  invisible(eq)
}


## eq_allom_param <- function(x, allom_param) {
##   stopifnot(length(allom_param) == 1 &&
##             allom_param %in% model_allom_params(x))
##   # Get fixed and random effects
##   fe <- x$formulas$fixed[[allom_param]]
##   re <- x$formulas$random[[allom_param]]
  
## }



extract_eq2 <- function(x, ...) {
  args <- list(...)
  eq_args <- list(parts = c("core", "response", "a", "b", "s_log", "s_lin"),
                  core_order = c("generative", "reversed"),
                  effects = c("fixed", "random"),
                  notation = c("indexed", "matrix"),
                  priors = c(FALSE, TRUE),
                  verbosity = c("low", "medium", "high"))
  display <- tibble::tibble(
               part = eq_args[["parts"]],
               generative = FALSE,
               fe = FALSE,
               re = FALSE)
  # Build table containing all the equations bits

  equations <- tibble::tibble(
               part = eq_args[["parts"]],
               generative = rep(list(NULL), length(eq_args[["parts"]])),
               fe = rep(list(NULL), length(eq_args[["parts"]])),
               re = rep(list(NULL), length(eq_args[["parts"]])))
                         
  # Set cells of display table to TRUE based on the arguments
  
  # Build the output value based on the display table
  eq <- c()
  
}

