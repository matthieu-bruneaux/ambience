[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/ambience/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/ambience/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/ambience/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/ambience/coverage/coverage.html)
[![R CMD check](https://matthieu-bruneaux.gitlab.io/ambience/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/ambience/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

# The ambience R package

## Bayesian analysis of allometric data with explicit measurement error

`ambience` is an R package aiming at making rigorous statistical analysis of allometric data as easy as possible.

It can model measurement error explicitly, thus allowing to keep in the analysis the negative observations which are sometimes observed for very small individuals. In this way, we avoid the estimation biases that can arise due to the non-random removal of negative observations.

## Quick install

You can install the package from the GitLab repository by typing from R:

```
devtools::install_gitlab("matthieu-bruneaux/ambience")
```

Please let us know if you encounter any issues during the package installation!

## Documentation

The package documentation is [available online](https://matthieu-bruneaux.gitlab.io/ambience/):

- If you want to begin with some light theory behind the model: [Why the ambience package?](articles/art-010-introduction.html)
- If you want to start right away with running a model: [Quick start](articles/art-020-modelling.html).

## Contact and feedback

Any feedback about the package is very welcome. Please don't hesitate to send us any comment or suggestion about `ambience`!

- [Matthieu Bruneaux](mailto:matthieu.bruneaux@gmail.com) (package maintainer)
- [Sophia Lambert](mailto:soph.lambert@outlook.fr)
- [Sandra Klemet-N'guessan](mailto:sandraklemet@trentu.ca)
- [Andrés López-Sepulcre](mailto:lopezsepulcre@gmail.com)
