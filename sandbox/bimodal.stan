data {
  int n;
  vector[n] y;
}

parameters {
  real mu;
  real<lower=0> sigma;
}

transformed parameters {
  real musq;
  musq = mu^2;
}

model {
  mu ~ normal(0, 10);
  y ~ normal(musq, sigma);
}
