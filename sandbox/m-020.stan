/// * Description

// Alpha, beta, sigma_mes (no covariates)

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters \alpha, \beta and \sigma_{mes}
// - customized priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - y_i \sim normal(exp(log(y_{pred,i})), \sigma_{mes})

// Prior codes:
// PRIOR_CODES <- c("constant" = 0,
//                  "uniform" = 1,
//                  "trun_normal" = 2)

/// * Data

data {
  // Observations  
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  // Priors
  int<lower=0,upper=2> prior_code_params[3]; // alpha, beta, sigma_mes
  int<lower=1,upper=3> prior_index_raw_param[3];
  
  int<lower=0> n_priors_constant;
  int<lower=0> n_priors_uniform;
  int<lower=0> n_priors_trunNormal;

  real prior_constant_params_value[3]; // 3 is the number of parameters
  real prior_uniform_params_low[3];
  real prior_uniform_params_high[3];
  real prior_trunNormal_params_mean[3];
  real prior_trunNormal_params_sd[3];
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  real<lower=0,upper=1> raw_uniform_params[n_priors_uniform];
  real<lower=0> raw_trunNormal_params[n_priors_trunNormal];
}

/// * Transformed parameters

transformed parameters {
  real<lower=0> params[3]; // alpha, beta, sigma_mes
  real<lower=0> alpha;
  real<lower=0> beta;
  real<lower=0> sigma_mes;
  vector[n_obs] log_y_pred;

  for (i in 1:3) {
    // Constant
    if (prior_code_params[i] == 0) {
      params[i] = prior_constant_params_value[i];
    }
    // Uniform
    if (prior_code_params[i] == 1) {
      params[i] = (prior_uniform_params_low[i] +
                   (prior_uniform_params_high[i] - prior_uniform_params_low[i]) *
                   raw_uniform_params[prior_index_raw_param[i]]);
    }
    // Truncated normal
    if (prior_code_params[i] == 2) {
      params[i] = raw_trunNormal_params[prior_index_raw_param[i]];
    }
  }
  
  alpha = params[1];
  beta = params[2];
  sigma_mes = params[3];

  log_y_pred = log(alpha) + beta * log_x;
}

/// * Model

model {

  /// ** Raw parameters
  for (i in 1:3) {
    if (prior_code_params[i] == 1) {
      raw_uniform_params[prior_index_raw_param[i]] ~ uniform(0, 1);
    }
    if (prior_code_params[i] == 2) {
      raw_trunNormal_params[prior_index_raw_param[i]] ~ normal(prior_trunNormal_params_mean[i], prior_trunNormal_params_sd[i]);
    }
  }
  
  /// ** Likelihood
  y ~ normal(exp(log_y_pred), sigma_mes);

}

/// * Generated quantities

generated quantities {
  // Log-likelihood
  vector[n_obs] log_lik;
  for (i in 1:n_obs) {
    log_lik[i] = normal_lpdf(y[i] | exp(log_y_pred[i]), sigma_mes);
  }
}
