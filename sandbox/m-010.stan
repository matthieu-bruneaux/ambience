/// * Description

// Alpha, beta, sigma_mes (no covariates)

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters \alpha, \beta and \sigma_{mes}
// - hard-coded priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - y_i \sim normal(exp(log(y_{pred,i})), \sigma_{mes})

/// * Data

data {
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  real<lower=0> alpha;
  real<lower=0> beta;
  real<lower=0> sigma_mes;
}

/// * Transformed parameters

transformed parameters {
  vector[n_obs] log_y_pred;
  log_y_pred = log(alpha) + beta * log_x;
}

/// * Model

model {

  /// ** Hard-coded priors
  alpha ~ normal(0, 10);
  beta ~ normal(0, 10);
  sigma_mes ~ normal(0, 10);
  
  /// ** Likelihood
  y ~ normal(exp(log_y_pred), sigma_mes);

}

/// * Generated quantities

generated quantities {
  // Log-likelihood
  vector[n_obs] log_lik;
  for (i in 1:n_obs) {
    log_lik[i] = normal_lpdf(y[i] | exp(log_y_pred[i]), sigma_mes);
  }
}
