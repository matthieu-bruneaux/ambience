data {
  int n;
  vector[n] y;
}

parameters {
  real mu1;
  real mu2;
  real mu3;
  real mu4;
  real<lower=0> sigma;
}

transformed parameters {
  real musq;
  musq = (mu1 + mu2 + mu3 + mu4)^2;
}

model {
  mu1 ~ normal(0, 10);
  mu2 ~ normal(0, 10);
  mu3 ~ normal(0, 10);
  mu4 ~ normal(0, 10);
  y ~ normal(musq, sigma);
}
