/// * Description

// Allometric parameters are a, b, s_log, s_lin.

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters a, b, \sigma_{log} and \sigma_{mes}
// - customized priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - log(y_{sample,i}) \sim normal(log(y_{pred,i}), \sigma_{log})
// - y_i \sim normal(exp(log(y_{sample,i})), \sigma_{lin})
// - alternatively, if use_tdist_lin is TRUE:
//   y_i \sim student_t(nu_lin, exp(log(y_{sample,i})), \sigma_{lin})

// For each primary parameter (a, b, sigma_log, sigma_lin), we use a log
// link function to model the effect of covariates. For example, for a:
// - log(a) = X_{a} \beta_{a}
// where log(a) is a n_{obs} \times 1 column-vector, X_{a} is a
// n_{obs} \times k design matrix, with k being the number of coefficients
// needed to describe the fixed effects (at least one for the intercept) and
// \beta_{a} is a n_{obs} \times 1 column-vector of coefficients.

// Prior codes:
// PRIOR_CODES <- c("constant" = 0,
//                  "uniform" = 1,
//                  "trun_normal" = 2,
//                  "hcauchy" = 3,
//                  "gamma" = 4)

/// * Functions

functions {

  /// * map_from_raw_params()

  // Build coefficients for fixed effects from raw parameters
  vector map_from_raw_params(int[] prior_type_codes, // Ribbon with all the prior types
                             int[] raw_param_indices, // Ribbon with all the sub-indices (within each prior type)
                             int start, int k, // start index and length of vector to extract, within the two previous ribbons
                             // Information to convert from raw to natural scale
                             real[] prior_constant_params_value,
                             real[] prior_uniform_params_low,
                             real[] prior_uniform_params_high,
                             real[] prior_trunNormal_params_mean,
                             real[] prior_trunNormal_params_sd,
                             real[] prior_hCauchy_params_scale,
                             real[] prior_gamma_params_shape,
                             real[] prior_gamma_params_rate,
                             // Actual raw parameters
                             real[] raw_uniform_params,
                             real[] raw_trunNormal_params,
                             real[] raw_hCauchy_params,
                             real[] raw_gamma_params
                             ) {
    vector[k] coeffs;
    int j;
    for (i in 1:k) {
      j = start + i - 1;
      // Constant
      if (prior_type_codes[j] == 0) {
        coeffs[i] = prior_constant_params_value[j];
      }
      // Uniform
      if (prior_type_codes[j] == 1) {
        coeffs[i] = (prior_uniform_params_low[j] +
                     (prior_uniform_params_high[j] - prior_uniform_params_low[j]) *
                     raw_uniform_params[raw_param_indices[j]]);
      }
      // Truncated normal
      if (prior_type_codes[j] == 2) {
        coeffs[i] = raw_trunNormal_params[raw_param_indices[j]];
      }
      // Half-Cauchy
      if (prior_type_codes[j] == 3) {
        coeffs[i] = (prior_hCauchy_params_scale[j] *
                     raw_hCauchy_params[raw_param_indices[j]]);
      }
      // Gamma
      if (prior_type_codes[j] == 4) {
        coeffs[i] = raw_gamma_params[raw_param_indices[j]];
      }
    }
    return(coeffs);
  }
  // End of map_from_raw_params()
  
}
  
/// * Data

data {
  // Observations  
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  // Use sigma_lin? (to include measurement error on the linear scale)
  int<lower=0, upper=1> use_sigma_lin;
  
  // Information to map raw parameters to a, b, s_log, and s_lin coefficients
  int k_a;
  int k_b;
  int k_s_log;
  int k_s_lin;
  int start_a;
  int start_b;
  int start_s_log;
  int start_s_lin;

  // Design matrices for fixed_effects
  matrix[n_obs,k_a] design_a;
  matrix[n_obs,k_b] design_b;
  matrix[n_obs,k_s_log] design_slog;
  matrix[n_obs,k_s_lin] design_slin;

  // Information for grouping factors (random effects on response variable)
  int<lower=0> K_response;            // number of grouping factors
  int<lower=1> L_k_response[K_response];       // number of levels for each grouping factor
  int<lower=K_response> sum_L_k_response;      // sum(L_k)
  int<lower=1> Z_response[n_obs,K_response];       // matrix containing the z_k as columns
  int<lower=1> gf_response[sum_L_k_response];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter a)
  int<lower=0> K_a;            // number of grouping factors
  int<lower=1> L_k_a[K_a];       // number of levels for each grouping factor
  int<lower=K_a> sum_L_k_a;      // sum(L_k)
  int<lower=1> Z_a[n_obs,K_a];       // matrix containing the z_k as columns
  int<lower=1> gf_a[sum_L_k_a];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter b)
  int<lower=0> K_b;            // number of grouping factors
  int<lower=1> L_k_b[K_b];       // number of levels for each grouping factor
  int<lower=K_b> sum_L_k_b;      // sum(L_k)
  int<lower=1> Z_b[n_obs,K_b];       // matrix containing the z_k as columns
  int<lower=1> gf_b[sum_L_k_b];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter s_log)
  int<lower=0> K_s_log;            // number of grouping factors
  int<lower=1> L_k_s_log[K_s_log];       // number of levels for each grouping factor
  int<lower=K_s_log> sum_L_k_s_log;      // sum(L_k)
  int<lower=1> Z_s_log[n_obs,K_s_log];       // matrix containing the z_k as columns
  int<lower=1> gf_s_log[sum_L_k_s_log];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter s_lin)
  int<lower=0> K_s_lin;            // number of grouping factors
  int<lower=1> L_k_s_lin[K_s_lin];       // number of levels for each grouping factor
  int<lower=K_s_lin> sum_L_k_s_lin;      // sum(L_k)
  int<lower=1> Z_s_lin[n_obs,K_s_lin];       // matrix containing the z_k as columns
  int<lower=1> gf_s_lin[sum_L_k_s_lin];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Priors
  int<lower=0> n_params; // Number of fixed-effect parameters
  int<lower=0,upper=4> prior_code_params[n_params];
  int<lower=1,upper=n_params> prior_index_raw_param[n_params];
  
  int<lower=0> n_priors_constant;
  int<lower=0> n_priors_uniform;
  int<lower=0> n_priors_trunNormal;
  int<lower=0> n_priors_hCauchy;
  int<lower=0> n_priors_gamma;

  real prior_constant_params_value[n_params]; // n_params is the number of parameters
  real prior_uniform_params_low[n_params];
  real prior_uniform_params_high[n_params];
  real prior_trunNormal_params_mean[n_params];
  real prior_trunNormal_params_sd[n_params];
  real prior_hCauchy_params_scale[n_params];
  real prior_gamma_params_shape[n_params];
  real prior_gamma_params_rate[n_params];
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  real<lower=0,upper=1> raw_uniform_params[n_priors_uniform];
  real<lower=0> raw_trunNormal_params[n_priors_trunNormal];
  real<lower=0> raw_hCauchy_params[n_priors_hCauchy];
  real<lower=0> raw_gamma_params[n_priors_gamma];
  vector[n_obs] log_y_sample_sc_res[use_sigma_lin];

  vector[sum_L_k_response] u_raw_response;
  vector<lower=0>[K_response] sigma_k_response;
  vector[sum_L_k_a] u_raw_a;
  vector<lower=0>[K_a] sigma_k_a;
  vector[sum_L_k_b] u_raw_b;
  vector<lower=0>[K_b] sigma_k_b;
  vector[sum_L_k_s_log] u_raw_s_log;
  vector<lower=0>[K_s_log] sigma_k_s_log;
  vector[sum_L_k_s_lin] u_raw_s_lin;
  vector<lower=0>[K_s_lin] sigma_k_s_lin;
}

/// * Transformed parameters

transformed parameters {

  // TODO: Refactor this in a more compact way using array of vectors/matrices of length 4?
  
  vector[k_a] fe_a;         // Coefficients for fixed effects on a
  vector[k_b] fe_b;         // Coefficients for fixed effects on b
  vector[k_s_log] fe_s_log; // Coefficients for fixed effects on s_log
  vector[k_s_lin] fe_s_lin; // Coefficients for fixed effects on s_lin
  vector[n_obs] log_a;
  vector[n_obs] log_b;
  vector[n_obs] log_s_log;
  vector[n_obs] log_s_lin;
  vector[n_obs] log_y_sample;
  vector[sum_L_k_response] u_response;
  vector[sum_L_k_a] u_a;
  vector[sum_L_k_b] u_b;
  vector[sum_L_k_s_log] u_s_log;
  vector[sum_L_k_s_lin] u_s_lin;
  vector[n_obs] y_mes;

  // Get coefficients from raw parameters
  fe_a = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_a, k_a, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale,
    prior_gamma_params_shape, prior_gamma_params_rate,
    raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params, raw_gamma_params);
  fe_b = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_b, k_b, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale,
    prior_gamma_params_shape, prior_gamma_params_rate,
    raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params, raw_gamma_params);
  fe_s_log = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_s_log, k_s_log, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale,
    prior_gamma_params_shape, prior_gamma_params_rate,
    raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params, raw_gamma_params);
  if (use_sigma_lin) {
    fe_s_lin = map_from_raw_params(prior_code_params,
                                   prior_index_raw_param, start_s_lin, k_s_lin, prior_constant_params_value,
                                   prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
                                   prior_trunNormal_params_sd, prior_hCauchy_params_scale,
                                   prior_gamma_params_shape, prior_gamma_params_rate,
                                   raw_uniform_params,
                                   raw_trunNormal_params, raw_hCauchy_params, raw_gamma_params);
  }

  // Calculate primary parameter values (alpha, beta, sigma_log, and sigma_lin)
  log_a = design_a * log(fe_a);
  log_b = design_b * log(fe_b);
  log_s_log = design_slog * log(fe_s_log);
  if (use_sigma_lin) {
    log_s_lin = design_slin * log(fe_s_lin);
  }

  // Add effects of grouping factors (on a)
  if (K_a > 0) {
    for (j in 1:sum_L_k_a) {
      u_a[j] = u_raw_a[j] * sigma_k_a[gf_a[j]];
    }
    for (i in 1:n_obs) {
      for (j in 1:K_a) {
        log_a[i] += u_a[Z_a[i,j]];
      }
    }
  }
  // Add effects of grouping factors (on b)
  if (K_b > 0) {
    for (j in 1:sum_L_k_b) {
      u_b[j] = u_raw_b[j] * sigma_k_b[gf_b[j]];
    }
    for (i in 1:n_obs) {
      for (j in 1:K_b) {
        log_b[i] += u_b[Z_b[i,j]];
      }
    }
  }
  // Add effects of grouping factors (on s_log)
  if (K_s_log > 0) {
    for (j in 1:sum_L_k_s_log) {
      u_s_log[j] = u_raw_s_log[j] * sigma_k_s_log[gf_s_log[j]];
    }
    for (i in 1:n_obs) {
      for (j in 1:K_s_log) {
        log_s_log[i] += u_s_log[Z_s_log[i,j]];
      }
    }
  }
  // Add effects of grouping factors (on s_lin)
  if (K_s_lin > 0) {
    for (j in 1:sum_L_k_s_lin) {
      u_s_lin[j] = u_raw_s_lin[j] * sigma_k_s_lin[gf_s_lin[j]];
    }
    for (i in 1:n_obs) {
      for (j in 1:K_s_lin) {
        log_s_lin[i] += u_s_lin[Z_s_lin[i,j]];
      }
    }
  }

  if (use_sigma_lin) {
    log_y_sample = log_a + exp(log_b) .* log_x + log_y_sample_sc_res[1] .* exp(log_s_log);
  } else {
    log_y_sample = log_a + exp(log_b) .* log_x;
  }

  y_mes = exp(log_y_sample);

  // Add effects of grouping factors (on the observed variable)
  if (K_response > 0) {
    for (j in 1:sum_L_k_response) {
      u_response[j] = u_raw_response[j] * sigma_k_response[gf_response[j]];
    }
    for (i in 1:n_obs) {
      for (j in 1:K_response) {
        y_mes[i] += u_response[Z_response[i,j]];
      }
    }
  }
   
}

/// * Model

model {

  /// ** Priors for raw parameters
  for (i in 1:n_params) {
    if (prior_code_params[i] == 1) {
      raw_uniform_params[prior_index_raw_param[i]] ~ uniform(0, 1);
    }
    if (prior_code_params[i] == 2) {
      raw_trunNormal_params[prior_index_raw_param[i]] ~ normal(prior_trunNormal_params_mean[i], prior_trunNormal_params_sd[i]);
    }
    if (prior_code_params[i] == 3) {
      raw_hCauchy_params[prior_index_raw_param[i]] ~ cauchy(0, 1);
    }
    if (prior_code_params[i] == 4) {
      raw_gamma_params[prior_index_raw_param[i]] ~ gamma(prior_gamma_params_shape[i],
                                                         prior_gamma_params_rate[i]);
    }
  }
  
  /// ** Likelihood
  if (use_sigma_lin) {
    log_y_sample_sc_res[1] ~ normal(0, 1);
    y ~ normal(y_mes, exp(log_s_lin));
  } else {
    // TODO Check the maths for line below
    log(y) ~ normal(log(y_mes), exp(log_s_log));
  }
  
  /// Random effects
  if (K_response > 0) {
    u_raw_response ~ std_normal();
    sigma_k_response ~ normal(0, 2); // TODO Add customized prior here
  }
  if (K_a > 0) {
    u_raw_a ~ std_normal();
    sigma_k_a ~ normal(0, 2); // TODO Add customized prior here
  }  
  if (K_b > 0) {
    u_raw_b ~ std_normal();
    sigma_k_b ~ normal(0, 2); // TODO Add customized prior here
  } 
  if (K_s_log > 0) {
    u_raw_s_log ~ std_normal();
    sigma_k_s_log ~ normal(0, 2); // TODO Add customized prior here
  }  
  if (K_s_lin > 0) {
    u_raw_s_lin ~ std_normal();
    sigma_k_s_lin ~ normal(0, 2); // TODO Add customized prior here
  }
  
}

/// * Generated quantities

generated quantities {
  // Log-likelihood
  vector[n_obs] log_lik;
  if (use_sigma_lin) {
      for (i in 1:n_obs) {
        log_lik[i] = normal_lpdf(y[i] | exp(log_y_sample[i]), exp(log_s_lin[i]));
      }
  } else {
    for (i in 1:n_obs) {
      log_lik[i] = normal_lpdf(log(y[i]) | log_y_sample[i], exp(log_s_log[i]));
    }
  }
}
