### * Description

# Script to test how robust the t-dist can be when arbitrary outliers are present

### * Packages

library(ambience)
library(tidyverse)
library(etalon)
library(future)
plan(multicore)
# When using "multisession", etalon somehow reloads for each core when running tidy_estimates or tidy_intervals?
# Could it be a memory issue?

# TODO: Check if this occurs with smaller number of rows in sim table. Maybe
# merging all the voluminous MCMC fits together at the end of the run goes
# above the available memory and the workers crash?

# But then the issue should occur with fit_model, not with tidy_estimates? Even
# though the call to tidy_estimates is the first done with the huge sim_table
# (with all the MCMC fits) probably shared across workers.

### * Functions

### ** Simulate dataset

sim_data <- function(a, b, s_log, s_lin, n_correct, n_outliers) {
  # Parameters
  n <- n_correct
  n_added_outliers <- n_outliers
  # Simulate data
  x <- 10^(runif(n, -1, 1))
  y_allom <- a * x^b
  y_latent <- exp(rnorm(n, log(y_allom), s_log))
  y <- rnorm(n, y_latent, s_lin)
  x_outliers <- 10^(runif(n_added_outliers, 0.1, 1))
  range_y_outliers <- extendrange(y, f = 0.2)
  y_outliers <- runif(n_added_outliers, min = range_y_outliers[1],
                      max = range_y_outliers[2])
  d <- tibble(x = c(x, x_outliers),
              y = c(y, y_outliers),
              outlier = rep(c(FALSE, TRUE), times = c(n, n_added_outliers)))
  d
}

### ** Fit model

fit_allom_norm <- function(dataset) {
  m <- allom(y ~ x, data = dataset,
             family_lin = "normal")
  r <- run_mcmc(m, chains = 1)
  r
}

fit_allom_tdist <- function(dataset) {
  m <- allom(y ~ x, data = dataset,
             family_lin = "student_t")
  r <- run_mcmc(m, chains = 1)
  r
}

### ** Tidy results

tidy_allom <- function(x, ...) {
  o <- tidy(x, ...)
  o$term[o$term == "a[(Intercept)]"] <- "a"
  o$term[o$term == "b[(Intercept)]"] <- "b"
  o
}

### * Main


z <- sim_table() %>%
  cross_parameters(a = c(1), b = c(0.6, 0.8),
                   s_log = c(0.05, 0.1),
                   s_lin = c(0.1, 0.5)) %>%
  cross_settings(n_correct = c(20, 40, 80),
                 n_outliers = c(2, 4, 8)) %>%
  replicate_sims(60)

z <- z %>%
  gen_data(sim_data)

z <- z %>%
  fit_and_tidy(fit_model = list(norm_dist = fit_allom_norm,
                                t_dist = fit_allom_tdist),
               tidy_estimates = list(tidy_allom),
               tidy_intervals = list(tidy_allom),
               conf_level = seq(0.1, 0.9, by = 0.1))

saveRDS(z, "toto-x.rds")

x <- readRDS("toto-x.rds")

w <- bind_cols(review(x), x[, "intervals"]) %>%
  unnest(intervals) %>%
  group_by(fit_tag, term, set_n_correct, set_n_outliers, conf_level) %>%
  summarize(correctness = mean(contains_true))

ggplot(w %>% filter(term == "a"), aes(x = conf_level, y = correctness, col = fit_tag)) +
  geom_abline(slope = 1, intercept = 0) + coord_equal() +
  geom_point() +
  geom_line() +
  facet_grid(set_n_correct ~ set_n_outliers, labeller = label_both)

ggplot(w %>% filter(term == "b"), aes(x = conf_level, y = correctness, col = fit_tag)) +
  geom_abline(slope = 1, intercept = 0) + coord_equal() +
  geom_point() +
  geom_line() +
  facet_grid(set_n_correct ~ set_n_outliers, labeller = label_both)

w <- bind_cols(review(x), x[, "intervals"]) %>%
  unnest(intervals) %>%
  group_by(fit_tag, term, set_n_correct, set_n_outliers, conf_level,
           true_s_log, true_s_lin) %>%
  summarize(correctness = mean(contains_true),
            avg_width = mean(conf_width))

ggplot(w %>% filter(term == "b"), aes(x = conf_level, y = correctness, col = fit_tag)) +
  geom_abline(slope = 1, intercept = 0) + coord_equal() +
  geom_point() +
  geom_line() +
  facet_grid(true_s_log + true_s_lin ~ set_n_correct + set_n_outliers, labeller = label_both)

w <- bind_cols(review(x), x[, "intervals"]) %>%
  unnest(intervals) %>%
  group_by(fit_tag, term, set_n_correct, set_n_outliers, conf_level,
           true_s_log, true_s_lin, true_b) %>%
  summarize(correctness = mean(contains_true),
            avg_width = mean(conf_width))

ggplot(w %>% filter(term == "b" & conf_level == 0.8),
       aes(x = true_b, y = avg_width, col = fit_tag)) +
  geom_point() +
  geom_line() +
  facet_grid(true_s_log + true_s_lin ~ set_n_correct + set_n_outliers, labeller = label_both)

w2 <- review(x) %>%
  mutate(delta_a = est_a - true_a,
         delta_b = est_b - true_b)

ggplot(w2, aes(true_a, delta_a, col = fit_tag)) +
  geom_hline(yintercept = 0) +
  geom_boxplot(aes(group = interaction(true_a, fit_tag))) +
  facet_grid(set_n_correct ~ set_n_outliers, labeller = label_both)

ggplot(w2, aes(true_b, delta_b, col = fit_tag)) +
  geom_hline(yintercept = 0) +
  geom_boxplot(aes(group = interaction(true_b, fit_tag))) +
  facet_grid(set_n_correct ~ set_n_outliers, labeller = label_both)
