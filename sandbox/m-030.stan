/// * Description

// Alpha, beta, sigma_log, sigma_mes (no covariates)

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters \alpha, \beta, \simga_{log} and \sigma_{mes}
// - customized priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - log(y_{sample,i}) \sim normal(log(y_{pred,i}), \sigma_{log})
// - y_i \sim normal(exp(log(y_{sample,i})), \sigma_{mes})

// Prior codes:
// PRIOR_CODES <- c("constant" = 0,
//                  "uniform" = 1,
//                  "trun_normal" = 2)

/// * Data

data {
  // Observations  
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  // Priors
  int<lower=0> n_params; // Number of parameters
  int<lower=0,upper=3> prior_code_params[n_params]; // alpha, beta, sigma_log, sigma_mes
  int<lower=1,upper=n_params> prior_index_raw_param[n_params];
  
  int<lower=0> n_priors_constant;
  int<lower=0> n_priors_uniform;
  int<lower=0> n_priors_trunNormal;
  int<lower=0> n_priors_hCauchy;

  real prior_constant_params_value[n_params]; // n_params is the number of parameters
  real prior_uniform_params_low[n_params];
  real prior_uniform_params_high[n_params];
  real prior_trunNormal_params_mean[n_params];
  real prior_trunNormal_params_sd[n_params];
  real prior_hCauchy_params_scale[n_params];
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  real<lower=0,upper=1> raw_uniform_params[n_priors_uniform];
  real<lower=0> raw_trunNormal_params[n_priors_trunNormal];
  real<lower=0> raw_hCauchy_params[n_priors_hCauchy];
  vector[n_obs] log_y_sample_sc_res;
}

/// * Transformed parameters

transformed parameters {
  real<lower=0> params[n_params]; // alpha, beta, sigma_log, sigma_mes
  real<lower=0> alpha;
  real<lower=0> beta;
  real<lower=0> sigma_log;
  real<lower=0> sigma_mes;
  vector[n_obs] log_y_sample;

  for (i in 1:n_params) {
    // Constant
    if (prior_code_params[i] == 0) {
      params[i] = prior_constant_params_value[i];
    }
    // Uniform
    if (prior_code_params[i] == 1) {
      params[i] = (prior_uniform_params_low[i] +
                   (prior_uniform_params_high[i] - prior_uniform_params_low[i]) *
                   raw_uniform_params[prior_index_raw_param[i]]);
    }
    // Truncated normal
    if (prior_code_params[i] == 2) {
      params[i] = raw_trunNormal_params[prior_index_raw_param[i]];
    }
    // Half-Cauchy
    if (prior_code_params[i] == 3) {
      params[i] = (prior_hCauchy_params_scale[i] *
                   raw_hCauchy_params[prior_index_raw_param[i]]);
    }
  }
  
  alpha = params[1];
  beta = params[2];
  sigma_log = params[3];
  sigma_mes = params[4];

  log_y_sample = log(alpha) + beta * log_x + log_y_sample_sc_res * sigma_log;
}

/// * Model

model {

  /// ** Raw parameters
  for (i in 1:3) {
    if (prior_code_params[i] == 1) {
      raw_uniform_params[prior_index_raw_param[i]] ~ uniform(0, 1);
    }
    if (prior_code_params[i] == 2) {
      raw_trunNormal_params[prior_index_raw_param[i]] ~ normal(prior_trunNormal_params_mean[i], prior_trunNormal_params_sd[i]);
    }
    if (prior_code_params[i] == 3) {
      raw_hCauchy_params[prior_index_raw_param[i]] ~ cauchy(0, 1);
    }
  }
  
  /// ** Likelihood
  log_y_sample_sc_res ~ normal(0, 1);
  y ~ normal(exp(log_y_sample), sigma_mes);

}

/// * Generated quantities

generated quantities {
  // Log-likelihood
  vector[n_obs] log_lik;
  for (i in 1:n_obs) {
    log_lik[i] = normal_lpdf(y[i] | exp(log_y_sample[i]), sigma_mes);
  }
}
