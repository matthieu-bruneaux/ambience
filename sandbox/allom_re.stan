/// * Description

// Note that no log_lik quantities are generated to keep the output small. See https://discourse.mc-stan.org/t/extract-log-likelihood-from-large-size-stanfit-using-the-loo-package/4396/2 for using the loo.function() approach.

// About random effect parameters: they act on the log scale for alpha, beta, sigma_log and sigma_mes.

/// * Data

data {
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  int<lower=1> n_fixed_effects[4]; // For alpha, beta, sigma_log, sigma_res (in this order)
  int<lower=0> n_random_effects[5]; // For response, alpha, beta, sigma_log, sigma_res (in this order)
  int<lower=0> n_random_levels_response[n_random_effects[1]+2]; // Doubly padded
  int<lower=0> n_random_levels_alpha[n_random_effects[2]+2];
  int<lower=0> n_random_levels_beta[n_random_effects[3]+2];
  int<lower=0> n_random_levels_sigma_log[n_random_effects[4]+2];
  int<lower=0> n_random_levels_sigma_mes[n_random_effects[5]+2];
  int<lower=0> max_random_levels[5]; // For response, alpha, beta, sigma_log, sigma_res (in this order)
  
  // Fixed effect coefficients for each observation
  matrix[n_obs, n_fixed_effects[1]] predictors_alpha;
  matrix[n_obs, n_fixed_effects[2]] predictors_beta;
  matrix[n_obs, n_fixed_effects[3]] predictors_sigma_log;
  matrix[n_obs, n_fixed_effects[4]] predictors_sigma_mes;

  // Random effect indices for each observation
  int<lower=0> random_effects_response[n_obs, n_random_effects[1]+1]; // dim+1 is for padding (in case no RE is present)
  int<lower=0> random_effects_alpha[n_obs, n_random_effects[2]+1];
  int<lower=0> random_effects_beta[n_obs, n_random_effects[3]+1];
  int<lower=0> random_effects_sigma_log[n_obs, n_random_effects[4]+1];
  int<lower=0> random_effects_sigma_mes[n_obs, n_random_effects[5]+1];
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  // Fixed effects
  vector<lower=0>[n_fixed_effects[1]] exp_coefs_alpha;
  vector<lower=0>[n_fixed_effects[2]] exp_coefs_beta;
  vector<lower=0>[n_fixed_effects[3]] exp_coefs_sigma_log;
  vector<lower=0>[n_fixed_effects[4]] exp_coefs_sigma_mes;
  // Random effects
  matrix[max_random_levels[1]+1, n_random_effects[1]+1] random_levels_response; // Padded
  matrix[max_random_levels[2]+1, n_random_effects[2]+1] random_levels_alpha;
  matrix[max_random_levels[3]+1, n_random_effects[3]+1] random_levels_beta;
  matrix[max_random_levels[4]+1, n_random_effects[4]+1] random_levels_sigma_log;
  matrix[max_random_levels[5]+1, n_random_effects[5]+1] random_levels_sigma_mes;
  // Random effect variances
  vector<lower=0> [max_random_levels[1]+1] random_variances_response; // Padded
  vector<lower=0> [max_random_levels[2]+1] random_variances_alpha;
  vector<lower=0> [max_random_levels[3]+1] random_variances_beta;
  vector<lower=0> [max_random_levels[4]+1] random_variances_sigma_log;
  vector<lower=0> [max_random_levels[5]+1] random_variances_sigma_mes;
  // Variability for predicted values (log scale, i.e. before measurement)
  vector[n_obs] residuals_log_y;
}

/// * Transformed parameters

transformed parameters {
  // Fixed effects
  vector[n_fixed_effects[1]] coefs_alpha;
  vector[n_fixed_effects[2]] coefs_beta;
  vector[n_fixed_effects[3]] coefs_sigma_log;
  vector[n_fixed_effects[4]] coefs_sigma_mes;
  // Realised values
  vector[n_obs] log_alpha;
  vector<lower=0>[n_obs] beta;
  vector<lower=0>[n_obs] sigma_log;
  vector<lower=0>[n_obs] sigma_mes;
  vector[n_obs] log_y_pred;

  // Calculate realised alpha for each row
  coefs_alpha = log(exp_coefs_alpha);
  log_alpha = predictors_alpha * coefs_alpha;
  if (n_random_effects[2] > 0) {
    for (j in 1:(n_random_effects[2])) {
      for (i in 1:n_obs) {
        log_alpha[i] += random_levels_alpha[random_effects_alpha[i, j],j] * random_variances_alpha[j];
      }
    }
  }
  // Calculate realised beta for each row
  coefs_beta = log(exp_coefs_beta);
  beta = exp(predictors_beta * coefs_beta);
  if (n_random_effects[3] > 0) {
    for (j in 1:(n_random_effects[3])) {
      for (i in 1:n_obs) {
        beta[i] *= exp(random_levels_beta[random_effects_beta[i, j],j]  * random_variances_beta[j]);
      }
    }
  }

  // Calculate realised sigma_log for each row
  coefs_sigma_log = log(exp_coefs_sigma_log);
  sigma_log = exp(predictors_sigma_log * coefs_sigma_log);
  if (n_random_effects[4] > 0) {
    for (j in 1:(n_random_effects[4])) {
      for (i in 1:n_obs) {
        sigma_log[i] *= exp(random_levels_sigma_log[random_effects_sigma_log[i, j],j] * random_variances_sigma_log[j]);
      }
    }
  }
  // Calculate realised sigma_mes for each row
  coefs_sigma_mes = log(exp_coefs_sigma_mes);
  sigma_mes = exp(predictors_sigma_mes * coefs_sigma_mes);
  if (n_random_effects[5] > 0) {
    for (j in 1:(n_random_effects[5])) {
      for (i in 1:n_obs) {
        sigma_mes[i] *= exp(random_levels_sigma_mes[random_effects_sigma_mes[i, j],j] * random_variances_sigma_mes[j]);
      }
    }
  }

  // Calculate log_y_pred for each row
  log_y_pred = log_alpha + beta .* log_x + residuals_log_y .* sigma_log; // .* is element-wise multiplication for vectors

}

/// * Model

model {
  
  /// ** Priors fixed effects
  exp_coefs_alpha[1] ~ normal(0, 10);
  if (n_fixed_effects[1]>1) {
    for (i in 2:(n_fixed_effects[1])) {
      exp_coefs_alpha[i] ~ lognormal(0, 1);
    }
  }
  exp_coefs_beta[1] ~ normal(0.7, 0.3);
  if (n_fixed_effects[2]>1) {
    for (i in 2:(n_fixed_effects[2])) {
      exp_coefs_beta[i] ~ lognormal(0, 1);
    }
  }
  exp_coefs_sigma_log[1] ~ cauchy(0, 2);
  if (n_fixed_effects[3]>1) {
    for (i in 2:(n_fixed_effects[3])) {
      exp_coefs_sigma_log[i] ~ lognormal(0, 1);
    }
  }
  exp_coefs_sigma_mes[1] ~ cauchy(0, 2);
  if (n_fixed_effects[4]>1) {
    for (i in 2:(n_fixed_effects[4])) {
      exp_coefs_sigma_mes[i] ~ lognormal(0, 1);
    }
  }

  /// ** Priors random effects
  for (i in 1:(max_random_levels[1]+1)) {
    random_levels_response[i, ] ~ normal(0, 1);
  }
  for (i in 1:(max_random_levels[2]+1)) {
    random_levels_alpha[i, ] ~ normal(0, 1);
  }
  for (i in 1:(max_random_levels[3]+1)) {
    random_levels_beta[i, ] ~ normal(0, 1);
  }
  for (i in 1:(max_random_levels[4]+1)) {
    random_levels_sigma_log[i, ] ~ normal(0, 1);
  }
  for (i in 1:(max_random_levels[5]+1)) {
    random_levels_sigma_mes[i, ] ~ normal(0, 1);
  }

  /// ** Priors random effect variances
  random_variances_response ~ normal(0, 1);
  random_variances_alpha ~ normal(0, 1);
  random_variances_beta ~ normal(0, 1);
  random_variances_sigma_log ~ normal(0, 1);
  random_variances_sigma_mes ~ normal(0, 1);
  
  /// ** Likelihood
  residuals_log_y ~ normal(0, 1);
  y ~ normal(exp(log_y_pred), sigma_mes);

}
