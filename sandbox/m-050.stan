/// * Description

// Alpha, beta, sigma_log, sigma_mes (fixed effects)

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters \alpha, \beta, \simga_{log} and \sigma_{mes}
// - customized priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - log(y_{sample,i}) \sim normal(log(y_{pred,i}), \sigma_{log})
// - y_i \sim normal(exp(log(y_{sample,i})), \sigma_{mes})

// For each primary parameter (alpha, beta, sigma_log, sigma_mes), we use a log
// link function to model the effect of covariates. For example, for alpha:
// - log(\alpha) = X_{\alpha} \gamma_{\alpha}
// where log(\alpha) is a n_{obs} \times 1 column-vector, X_{\alpha} is a
// n_{obs} \times k design matrix, with k being the number of coefficients
// needed to describe the fixed effects (at least one for the intercept) and
// \gamma_{\alpha} is a n_{obs} \times 1 column-vector of coefficients.

// Prior codes:
// PRIOR_CODES <- c("constant" = 0,
//                  "uniform" = 1,
//                  "trun_normal" = 2)

/// * Functions

functions {

  /// * map_from_raw_params()

  // Build coefficients for fixed effects from raw parameters
  vector map_from_raw_params(int[] prior_type_codes, // Ribbon with all the prior types
                             int[] raw_param_indices, // Ribbon with all the sub-indices (within each prior type)
                             int start, int k, // start index and length of vector to extract, within the two previous ribbons
                             // Information to convert from raw to natural scale
                             real[] prior_constant_params_value,
                             real[] prior_uniform_params_low,
                             real[] prior_uniform_params_high,
                             real[] prior_trunNormal_params_mean,
                             real[] prior_trunNormal_params_sd,
                             real[] prior_hCauchy_params_scale,
                             // Actual raw parameters
                             real[] raw_uniform_params,
                             real[] raw_trunNormal_params,
                             real[] raw_hCauchy_params
                             ) {
    vector[k] coeffs;
    int j;
    for (i in 1:k) {
      j = start + i - 1;
      // Constant
      if (prior_type_codes[j] == 0) {
        coeffs[i] = prior_constant_params_value[j];
      }
      // Uniform
      if (prior_type_codes[j] == 1) {
        coeffs[i] = (prior_uniform_params_low[j] +
                     (prior_uniform_params_high[j] - prior_uniform_params_low[j]) *
                     raw_uniform_params[raw_param_indices[j]]);
      }
      // Truncated normal
      if (prior_type_codes[j] == 2) {
        coeffs[i] = raw_trunNormal_params[raw_param_indices[j]];
      }
      // Half-Cauchy
      if (prior_type_codes[j] == 3) {
        coeffs[i] = (prior_hCauchy_params_scale[j] *
                     raw_hCauchy_params[raw_param_indices[j]]);
      }
    }
    return(coeffs);
  }
  // End of map_from_raw_params()
  
}
  
/// * Data

data {

  real<lower=0> sigma_log_dummy_sd;
  real<lower=0> sigma_mes_dummy_sd;
  

  // Observations  
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  // Information to map raw parameters to alpha, beta, sigma_log, and sigma_mes coefficients
  int k_alpha;
  int k_beta;
  int k_sigma_log;
  int k_sigma_mes;
  int start_alpha;
  int start_beta;
  int start_sigma_log;
  int start_sigma_mes;

  // Design matrices for fixed_effects
  matrix[n_obs,k_alpha] design_alpha;
  matrix[n_obs,k_beta] design_beta;
  matrix[n_obs,k_sigma_log] design_sigma_log;
  matrix[n_obs,k_sigma_mes] design_sigma_mes;
  
  // Priors
  int<lower=0> n_params; // Number of fixed-effect parameters
  int<lower=0,upper=3> prior_code_params[n_params];
  int<lower=1,upper=n_params> prior_index_raw_param[n_params];
  
  int<lower=0> n_priors_constant;
  int<lower=0> n_priors_uniform;
  int<lower=0> n_priors_trunNormal;
  int<lower=0> n_priors_hCauchy;

  real prior_constant_params_value[n_params]; // n_params is the number of parameters
  real prior_uniform_params_low[n_params];
  real prior_uniform_params_high[n_params];
  real prior_trunNormal_params_mean[n_params];
  real prior_trunNormal_params_sd[n_params];
  real prior_hCauchy_params_scale[n_params];
}

transformed data {
  vector[n_obs] log_x;
  log_x = log(x);
}

/// * Parameters

parameters {
  real<lower=0,upper=1> raw_uniform_params[n_priors_uniform];
  real<lower=0> raw_trunNormal_params[n_priors_trunNormal];
  real<lower=0> raw_hCauchy_params[n_priors_hCauchy];
  real<lower=0> sigma_log_dummy;
  real<lower=0> sigma_mes_dummy;
  vector[n_obs] log_y_sample_sc_res;
}

/// * Transformed parameters

transformed parameters {

  // TODO: Refactor this in a more compact way using array of vectors/matrices of length 4?
  
  vector[k_alpha] coeffs_alpha;
  vector[k_beta] coeffs_beta;
  vector[k_sigma_log] coeffs_sigma_log;
  vector[k_sigma_mes] coeffs_sigma_mes;
  vector[n_obs] log_alpha;
  vector[n_obs] log_beta;
  vector[n_obs] log_sigma_log;
  vector[n_obs] log_sigma_mes;
  vector[n_obs] log_y_sample;

  // Get coefficients from raw parameters
  coeffs_alpha = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_alpha, k_alpha, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale, raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params);
  coeffs_beta = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_beta, k_beta, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale, raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params);
  coeffs_sigma_log = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_sigma_log, k_sigma_log, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale, raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params);
  coeffs_sigma_mes = map_from_raw_params(prior_code_params,
    prior_index_raw_param, start_sigma_mes, k_sigma_mes, prior_constant_params_value,
    prior_uniform_params_low, prior_uniform_params_high, prior_trunNormal_params_mean,
    prior_trunNormal_params_sd, prior_hCauchy_params_scale, raw_uniform_params,
    raw_trunNormal_params, raw_hCauchy_params);

  // Calculate primary parameter values (alpha, beta, sigma_log, and sigma_mes)
  log_alpha = design_alpha * log(coeffs_alpha);
  log_beta = design_beta * log(coeffs_beta);
  log_sigma_log = design_sigma_log * log(coeffs_sigma_log);
  log_sigma_mes = design_sigma_mes * log(coeffs_sigma_mes);
  
  log_y_sample = log_alpha + exp(log_beta) .* log_x + log_y_sample_sc_res .* (exp(log_sigma_log) + sigma_log_dummy);
}

/// * Model

model {

  /// ** Priors for raw parameters
  sigma_log_dummy ~ normal(0, sigma_log_dummy_sd);
  sigma_mes_dummy ~ normal(0, sigma_mes_dummy_sd);
  for (i in 1:n_params) {
    if (prior_code_params[i] == 1) {
      raw_uniform_params[prior_index_raw_param[i]] ~ uniform(0, 1);
    }
    if (prior_code_params[i] == 2) {
      raw_trunNormal_params[prior_index_raw_param[i]] ~ normal(prior_trunNormal_params_mean[i], prior_trunNormal_params_sd[i]);
    }
    if (prior_code_params[i] == 3) {
      raw_hCauchy_params[prior_index_raw_param[i]] ~ cauchy(0, 1);
    }
  }
  
  /// ** Likelihood
  log_y_sample_sc_res ~ normal(0, 1);
  y ~ normal(exp(log_y_sample), exp(log_sigma_mes) + sigma_mes_dummy);

}

/// * Generated quantities

generated quantities {
  real sigma_log_sum;
  real sigma_mes_sum;
  // Log-likelihood
  vector[n_obs] log_lik;
  for (i in 1:n_obs) {
    log_lik[i] = normal_lpdf(y[i] | exp(log_y_sample[i]), exp(log_sigma_mes[i]) + sigma_mes_dummy);
  }
  sigma_log_sum = coeffs_sigma_log[1] + sigma_log_dummy;
  sigma_mes_sum = coeffs_sigma_mes[1] + sigma_mes_dummy;
}
