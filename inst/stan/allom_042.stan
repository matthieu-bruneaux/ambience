/// * Description

// TODO Review the maths in the whole file.

// TODO Check
// https://github.com/stan-dev/stan/wiki/Prior-Choice-Recommendations about
// non-independence between nu and sigma in student's t dist and an alternative
// parameterization.

// Allometric parameters are a, b, s_log, s_lin.

// Model:
// - n_obs observations with predictor x and measurements y
// - parameters a, b, \sigma_{log} and \sigma_{mes}
// - customized priors for the parameters
// - \forall i \in [1,n_{obs}], log(y_{pred,i}) = log(\alpha) + \beta * log(x_i)
// - log(y_{sample,i}) \sim normal(log(y_{pred,i}), \sigma_{log})
// - y_i \sim normal(exp(log(y_{sample,i})), \sigma_{lin})
// - alternatively, if use_tdist_lin is TRUE:
//   y_i \sim student_t(nu_lin, exp(log(y_{sample,i})), \sigma_{lin})

// For each primary parameter (a, b, sigma_log, sigma_lin), we use a log
// link function to model the effect of covariates. For example, for a:
// - log(a) = X_{a} \beta_{a}
// where log(a) is a n_{obs} \times 1 column-vector, X_{a} is a
// n_{obs} \times k design matrix, with k being the number of coefficients
// needed to describe the fixed effects (at least one for the intercept) and
// \beta_{a} is a n_{obs} \times 1 column-vector of coefficients.

// Prior codes:
// PRIOR_CODES <- c("constant" = 0,
//                  "uniform" = 1,
//                  "trun_normal" = 2,
//                  "hcauchy" = 3,
//                  "gamma" = 4,
//                  "lognormal" = 5)

/// * Data

data {
  // Observations  
  int<lower=1> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y;

  // Switches
  /// Use sigma_lin? (to include measurement error on the linear scale)
  int<lower=0, upper=1> use_sigma_lin;
  /// Use normal or t-dist distribution for responses?
  int<lower=0, upper=1> family_log; // 0 = normal, 1 = t-dist
  int<lower=0, upper=1> family_lin; // 0 = normal, 1 = t-dist
  real nu_lower;
  
  // Mapping from raw parameters to coefficients for fixed effects (and nu
  // values for t distributions)
  /// Number of parameters for each
  int fe_k_a;
  int fe_k_b;
  int fe_k_s_log;
  int fe_k_s_lin;
  int fe_k_nu_log;
  int fe_k_nu_lin;
  /// Parameters indices
  int fe_i_a[fe_k_a];
  int fe_i_b[fe_k_b];
  int fe_i_s_log[fe_k_s_log];
  int fe_i_s_lin[fe_k_s_lin];
  int fe_i_nu_log[fe_k_nu_log];
  int fe_i_nu_lin[fe_k_nu_lin];

  // Design matrices for fixed_effects
  matrix[n_obs,fe_k_a] design_a;
  matrix[n_obs,fe_k_b] design_b;
  matrix[n_obs,fe_k_s_log] design_slog;
  matrix[n_obs,fe_k_s_lin] design_slin;

  // Information for grouping factors (random effects on response variable)
  int<lower=0> K_response;            // number of grouping factors
  int<lower=1> L_k_response[K_response];       // number of levels for each grouping factor
  int<lower=K_response> sum_L_k_response;      // sum(L_k)
  int<lower=1> Z_response[n_obs,K_response];       // matrix containing the z_k as columns
  int<lower=1> gf_response[sum_L_k_response];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter a)
  int<lower=0> K_a;            // number of grouping factors
  int<lower=1> L_k_a[K_a];       // number of levels for each grouping factor
  int<lower=K_a> sum_L_k_a;      // sum(L_k)
  int<lower=1> Z_a[n_obs,K_a];       // matrix containing the z_k as columns
  int<lower=1> gf_a[sum_L_k_a];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter b)
  int<lower=0> K_b;            // number of grouping factors
  int<lower=1> L_k_b[K_b];       // number of levels for each grouping factor
  int<lower=K_b> sum_L_k_b;      // sum(L_k)
  int<lower=1> Z_b[n_obs,K_b];       // matrix containing the z_k as columns
  int<lower=1> gf_b[sum_L_k_b];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter s_log)
  int<lower=0> K_s_log;            // number of grouping factors
  int<lower=1> L_k_s_log[K_s_log];       // number of levels for each grouping factor
  int<lower=K_s_log> sum_L_k_s_log;      // sum(L_k)
  int<lower=1> Z_s_log[n_obs,K_s_log];       // matrix containing the z_k as columns
  int<lower=1> gf_s_log[sum_L_k_s_log];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Information for grouping factors (random effects on allometric parameter s_lin)
  int<lower=0> K_s_lin;            // number of grouping factors
  int<lower=1> L_k_s_lin[K_s_lin];       // number of levels for each grouping factor
  int<lower=K_s_lin> sum_L_k_s_lin;      // sum(L_k)
  int<lower=1> Z_s_lin[n_obs,K_s_lin];       // matrix containing the z_k as columns
  int<lower=1> gf_s_lin[sum_L_k_s_lin];  // a vector giving the grouping factor index for j in [1..sum_L_k]
  
  // Mapping from raw parameters to sigmas for random effects
  /// Parameters indices
  int re_i_response[K_response];
  int re_i_a[K_a];
  int re_i_b[K_b];
  int re_i_s_log[K_s_log];
  int re_i_s_lin[K_s_lin];

  // Priors
  /// Number of fixed-effect parameters
  int<lower=0> n_params;
  /// We expect n_params == n_priors_constant + n_priors_uniform + ... + n_priors_gamma
  int<lower=0> n_priors_constant;
  int<lower=0> n_priors_uniform;
  int<lower=0> n_priors_trunNormal;
  int<lower=0> n_priors_hCauchy;
  int<lower=0> n_priors_gamma;
  int<lower=0> n_priors_lognormal;
  /// Parameters defining the prior distributions
  vector[n_priors_constant] prior_constant_params_value;
  vector[n_priors_uniform] prior_uniform_params_low;
  vector[n_priors_uniform] prior_uniform_params_high;
  vector[n_priors_trunNormal] prior_trunNormal_params_mean;
  vector[n_priors_trunNormal] prior_trunNormal_params_sd;
  vector[n_priors_lognormal] prior_lognormal_params_mean;
  vector[n_priors_lognormal] prior_lognormal_params_sd;
  vector[n_priors_hCauchy] prior_hCauchy_params_scale;
  vector[n_priors_gamma] prior_gamma_params_shape;
  vector[n_priors_gamma] prior_gamma_params_rate;
}

transformed data {
  vector[n_obs] log_x;
  int<lower=0,upper=1> use_tdist_log;
  int<lower=0,upper=1> use_tdist_lin;
  //
  log_x = log(x);
  use_tdist_log = family_log ? 1 : 0;
  use_tdist_lin = family_lin ? 1 : 0;
}

/// * Parameters

parameters {
  // Model parameters for fixed effects
  vector<lower=0,upper=1>[n_priors_uniform] raw_uniform_params;
  vector<lower=0>[n_priors_trunNormal] raw_trunNormal_params;
  vector<lower=0>[n_priors_lognormal] raw_lognormal_params;
  vector<lower=0>[n_priors_hCauchy] raw_hCauchy_params;
  vector<lower=0>[n_priors_gamma] raw_gamma_params;
  // Model latent variables
  vector[n_obs] log_y_sample_sc_res[use_sigma_lin];
  // Model parameters for random effects
  vector[sum_L_k_response] u_raw_response;
  vector[sum_L_k_a] u_raw_a;
  vector[sum_L_k_b] u_raw_b;
  vector[sum_L_k_s_log] u_raw_s_log;
  vector[sum_L_k_s_lin] u_raw_s_lin;
}

/// * Transformed parameters

transformed parameters {

  // TODO: Refactor this in a more compact way using array of vectors/matrices
  // of length 4?

  // Coefficients for fixed effects
  vector[fe_k_a] fe_a;
  vector[fe_k_b] fe_b;
  vector[fe_k_s_log] fe_s_log;
  vector[fe_k_s_lin] fe_s_lin;

  // Sigmas for random effects
  vector<lower=0>[K_response] sigma_k_response;
  vector<lower=0>[K_a] sigma_k_a;
  vector<lower=0>[K_b] sigma_k_b;
  vector<lower=0>[K_s_log] sigma_k_s_log;
  vector<lower=0>[K_s_lin] sigma_k_s_lin;

  // Others
  vector[n_obs] log_a;
  vector[n_obs] log_b;
  vector[n_obs] log_s_log;
  vector[n_obs] log_s_lin;
  vector[n_obs] log_y_sample;
  vector[sum_L_k_response] u_response;
  vector[sum_L_k_a] u_a;
  vector[sum_L_k_b] u_b;
  vector[sum_L_k_s_log] u_s_log;
  vector[sum_L_k_s_lin] u_s_lin;
  vector[n_obs] y_mes;
  ///
  vector[n_obs] tdist_nu_log;
  vector[n_obs] tdist_nu_lin;
  ///
  vector[n_priors_uniform] uniform_params;
  vector[n_priors_trunNormal] trunNormal_params;
  vector[n_priors_lognormal] lognormal_params;
  vector[n_priors_hCauchy] hCauchy_params;
  vector[n_priors_gamma] gamma_params;
  ///
  vector[n_params] all_params;

  // Shift/scale raw parameters
  uniform_params = (raw_uniform_params .*
                    (prior_uniform_params_high - prior_uniform_params_low) +
                    prior_uniform_params_low);
  trunNormal_params = raw_trunNormal_params;
  lognormal_params = raw_lognormal_params;
  hCauchy_params = raw_hCauchy_params .* prior_hCauchy_params_scale;
  gamma_params = raw_gamma_params;

  // Concatenate all shifted/scaled parameters (also including constant ones)
  all_params = append_row(append_row(append_row(append_row(prior_constant_params_value,
                                                uniform_params),
                                     append_row(trunNormal_params,
                                                hCauchy_params)),
                                     gamma_params), lognormal_params);
  // all_params is [n_params] long, and is a concatenation of constant params,
  // then uniform params, then trunNormal params, then hCauchy params, then
  // gamma params, then lognormal (in this order).
  
  // Get coefficients for fixed effects and nu values from raw parameters
  fe_a = all_params[fe_i_a];
  fe_b = all_params[fe_i_b];
  fe_s_log = all_params[fe_i_s_log];
  if (use_sigma_lin) { fe_s_lin = all_params[fe_i_s_lin]; }
  if (use_tdist_log) { tdist_nu_log = rep_vector(all_params[fe_i_nu_log[1]], n_obs); }
  if (use_tdist_lin) { tdist_nu_lin = rep_vector(all_params[fe_i_nu_lin[1]], n_obs); }

  // Get sigmas for random effects from raw parameters
  if (K_response) { sigma_k_response = all_params[re_i_response]; }
  if (K_a) { sigma_k_a = all_params[re_i_a]; }
  if (K_b) { sigma_k_b = all_params[re_i_b]; }
  if (K_s_log) { sigma_k_s_log = all_params[re_i_s_log]; }
  if (K_s_lin) { sigma_k_s_lin = all_params[re_i_s_lin]; }
    
  // Calculate primary parameter values (alpha, beta, sigma_log, and sigma_lin)
  log_a = design_a * log(fe_a);
  log_b = design_b * log(fe_b);
  log_s_log = design_slog * log(fe_s_log);
  if (use_sigma_lin) { log_s_lin = design_slin * log(fe_s_lin); }

  // Add effects of grouping factors (on a)
  if (K_a > 0) {
    u_a = u_raw_a .* sigma_k_a[gf_a];
    for (j in 1:K_a) { log_a += u_a[Z_a[,j]]; }
  }
  // Add effects of grouping factors (on b)
  if (K_b > 0) {
    u_b = u_raw_b .* sigma_k_b[gf_b];
    for (j in 1:K_b) { log_b += u_b[Z_b[,j]]; }
  }
  // Add effects of grouping factors (on s_log)
  if (K_s_log > 0) {
    u_s_log = u_raw_s_log .* sigma_k_s_log[gf_s_log];
    for (j in 1:K_s_log) { log_s_log += u_s_log[Z_s_log[,j]]; }
  }
  // Add effects of grouping factors (on s_lin)
  if (K_s_lin > 0) {
    u_s_lin = u_raw_s_lin .* sigma_k_s_lin[gf_s_lin];
    for (j in 1:K_s_lin) { log_s_lin += u_s_lin[Z_s_lin[,j]]; }
  }

  log_y_sample = log_a + exp(log_b) .* log_x;
  if (use_sigma_lin) {
    log_y_sample = log_y_sample + log_y_sample_sc_res[1] .* exp(log_s_log);
  }

  y_mes = exp(log_y_sample);

  // Add effects of grouping factors (on the observed variable)
  if (K_response > 0) {
    u_response = u_raw_response .* sigma_k_response[gf_response];
    for (j in 1:K_response) {
      y_mes += u_response[Z_response[,j]];
    }
  }
   
}

/// * Model

model {

  /// ** Priors for raw parameters
  if (n_priors_uniform) { raw_uniform_params ~ uniform(0, 1); }
  if (n_priors_trunNormal) { raw_trunNormal_params ~ normal(prior_trunNormal_params_mean,
                                                            prior_trunNormal_params_sd); }
  if (n_priors_hCauchy) { raw_hCauchy_params ~ cauchy(0, 1); }
  if (n_priors_gamma) { raw_gamma_params ~ gamma(prior_gamma_params_shape,
                                                 prior_gamma_params_rate); }
  if (n_priors_lognormal) { raw_lognormal_params ~ lognormal(prior_lognormal_params_mean,
                                                             prior_lognormal_params_sd); }

  /// ** Likelihood
  if (use_sigma_lin) {
    if (use_tdist_log) {
      log_y_sample_sc_res[1] ~ student_t(tdist_nu_log + nu_lower, 0, 1);
    } else {
      log_y_sample_sc_res[1] ~ normal(0, 1);
    }
    if (use_tdist_lin) {
      y ~ student_t(tdist_nu_lin + nu_lower, y_mes, exp(log_s_lin));
    } else {
      y ~ normal(y_mes, exp(log_s_lin));
    }
  } else {
    // TODO Check the maths for line below
    // TODO Check if some Jacobian correction must be included here
    if (use_tdist_log) {
      log(y) ~ student_t(tdist_nu_log + nu_lower, log(y_mes), exp(log_s_log));
    } else {
      log(y) ~ normal(log(y_mes), exp(log_s_log));
    }
  }

  /// Random effects
  if (K_response) { u_raw_response ~ std_normal(); }
  if (K_a) { u_raw_a ~ std_normal(); }
  if (K_b) { u_raw_b ~ std_normal(); }
  if (K_s_log) { u_raw_s_log ~ std_normal(); }
  if (K_s_lin) { u_raw_s_lin ~ std_normal(); }

}

/// * Generated quantities

generated quantities {
  vector[n_obs] log_lik;
  vector[use_tdist_log] nu_log;
  vector[use_tdist_lin] nu_lin;
  // Parameters expected from R
  if (use_tdist_log) { nu_log[1] = tdist_nu_log[1]; }
  if (use_tdist_lin) { nu_lin[1] = tdist_nu_lin[1]; }
  // Log-likelihood
  if (use_sigma_lin) {
    if (use_tdist_lin) {
      for (i in 1:n_obs) {
        log_lik[i] = student_t_lpdf(y[i] | tdist_nu_lin[i] + nu_lower, y_mes[i], exp(log_s_lin[i]));
      }
    } else {
      for (i in 1:n_obs) {
        log_lik[i] = normal_lpdf(y[i] | y_mes[i], exp(log_s_lin[i]));
      }
    }
  } else {
    if (use_tdist_log) {
      for (i in 1:n_obs) {
        log_lik[i] = student_t_lpdf(log(y[i]) | tdist_nu_log[i] + nu_lower, log(y_mes[i]), exp(log_s_log[i]));
      }
    } else {
      for (i in 1:n_obs) {
        log_lik[i] = normal_lpdf(log(y[i]) | log(y_mes[i]), exp(log_s_log[i]));
      }
    }
  }
}
