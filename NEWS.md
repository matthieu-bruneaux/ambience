# ambience 0.0.0.9009 (xxxx-xx-xx)

# ambience 0.0.0.9008 (2023-05-17)

## Package datasets

- Add a simulated dataset containing gryphon populations structure.

## Documentation

- Add a tutorial vignette demonstrating how to estimate population-level total response using the `posterior_predict()` function.

## Fixes/improvements

- Fix/update contributors information.
- Fix several issues raised by R CMD check.
- Decrease package size by optimizing vignette format (using jpeg figures and `rmarkdown::html_vignette` output format).

# ambience 0.0.0.9007 (2023-04-17)

## New features

- Complete working prototype of `extract_eq()` function.
- Add log-normal prior.

## Documentation

- Complete most of the planned vignettes.

## Improvements

- Make `posterior_predict.allomfit()` able to return predictions for only a given allometric parameter.
- Rename variables in gryphons and salmon datasets to simplify their use.
- Add more tests.

# ambience 0.0.0.9006 (2023-04-10)

Sandra Klemet-N'guessan is now a contributor to the package!

## Stan model improvements

- Implement Student's t distribution as an option for the model stochastic processes both on the log scale and on the linear scale.
- Take into account user-specified priors for random effects.
- Refactor the Stan code to make it more readable.

## Minor R code improvements

- Reexport functions from other packages cleanly.
- Add a `model_matrix()` function.
- Rename `coding_matrices()` to `coding_matrix()` for consistency.
- Improve examples.
- Add more tests.

## General documentation overhaul (work in progress)

- Reorganize tutorial vignettes to follow a more pedagogical progression for new users (but at this stage many vignettes still only have a title and need to be written).
- Add a vignette dedicated to Stan warnings.

## Upcoming features

- Starting to work on an `extract_eq()` function.

# ambience 0.0.0.9005 (2023-01-23)

## New vignette

- Add a case study vignette (based on simulated data) showing how to calculate population-level estimates from an allometric fit, when demographic data is available. It also demonstrates the effect of ignoring negative values during the allometric fit on those population-level estimates.

# ambience 0.0.0.9004 (2022-12-03)

## New dataset and vignette

- Add dataset of metabolic rates in juvenile Atlantic salmon, taken from Prokkola et al. (2021).
- Add a case study vignette about the salmon dataset (work in progress).

## Improvements

- Improve behaviour and documentation of `posterior_predict()` method for `allomfit` objects.
- Improve parameter labelling in `traceplot()` figures.
- Generally improve datasets and functions documentation.
- Polish list of exported functions.
- Generally polish vignettes.

## Others

- Change license from "GPL-3" to "GPL (>= 3)".
- Fix several R CMD check issues.

# ambience 0.0.0.9003 (2022-11-11)

## Breaking changes

- Rename primary allometric parameters from `alpha`, `beta`, `sigma_log` and `sigma_lin` to `a`, `b`, `s_log` and `s_lin`.
- Remove the option of using Student's t-distribution for variation on the linear scale (was introduced in the previous version).

## New features

- Implement categorical random effects for the response variable and all the primary allometric parameters.
- Add option to customize the contrasts used to build the design matrices of fixed effects.
- Add getter functions (`coding_matrices()`, `priors()`, `formula.allom()`).
- Work in progress to improve posterior predictions: allow new data in `posterior_predict_allomfit()` and add a new function `pp_allom_param()` to generate posteriors for primary allometric parameters based on new data.

## Dataset additions

- Add simulated datasets with fixed and random effects.
- Add gryphons model and MCMC run to package datasets.

## Other improvements

- Refactor code used to build models and to prepare and parse MCMC runs.
- Improve test suite.

# ambience 0.0.0.9002 (2022-01-18)

- Student's t-distribution now available to describe variation of the linear scale (in addition to the default normal distribution).

# ambience 0.0.0.9001 (2021-03-20)

- First version of ambience with a version number > 0.0.0.9000.
- `allom()` model working, with fixed effects implemented for all primary parameters ($\alpha$, $\beta$, $\sigma_{log}$, $\sigma_{lin}$).
- A small set of distributions are available for customized priors.
- Basic infrastructure for posterior predictive checks.

