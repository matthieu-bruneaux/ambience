# 2022-01-19

- Several vignettes in this folder are quite long to run, and some might even run processes in parallel for efficiency. This can lead to issues when running `R CMD check`.
- One solution, as suggested by a [post](https://blog.r-hub.io/2020/06/03/vignettes/) by Maëlle Salmon, is to pre-compute vignettes. Maëlle's post links to Jeroen Ooms's [post](https://ropensci.org/blog/2019/12/08/precompute-vignettes/) with detailed explanations about how to do it.
- As noted by Maëlle, this raises the question of testing and reproducibility. She cites [Henrik Bengtsson](https://www.mail-archive.com/r-package-devel@r-project.org/msg00812.html) who argues that testing can still be good if tests in `tests/` are good. For reproducibility, Henrik suggests to include the source vignettes in the package in `inst/full-vignettes/`.

# TODO

- A vignette with simulations, showing the potential issues that can arise if the size range is not wide enough? In this case, ambience might not be the best tool for the job?

- A vignette "To go further", describing some models that ambience cannot handle, and encouraging the user to use Stan to write their own customized model. Maybe a few Stan models demonstrating simple ambience fit could help? (The general Stan model of the package would be too complicated to use as a reference, because it is written to be able to handle a variety of cases.)
