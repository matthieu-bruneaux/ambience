data {
  int<lower=0> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y_obs;
}

parameters {
  real<lower=0> a;
  real<lower=0> b;
  real<lower=0> sigma_lin;
  real<lower=0> sigma_log;
  vector[n_obs] log_y_latent_raw;
}

transformed parameters {
  vector[n_obs] log_y_pred = log(a) + b * log(x);
  vector[n_obs] log_y_latent = log_y_latent_raw * sigma_log + log_y_pred;
}

model {
  log_y_latent_raw ~ std_normal();
  y_obs ~ normal(exp(log_y_latent), sigma_lin);
  a ~ normal(1, 1);
  b ~ normal(0.7, 0.2);
  sigma_log ~ normal(0, 1);
  sigma_lin ~ normal(0, 1);
}
