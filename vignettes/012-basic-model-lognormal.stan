data {
  int<lower=0> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y_obs;
}

parameters {
  real<lower=0> a;
  real<lower=0> b;
  real<lower=0> sigma_lin;
  real<lower=0> sigma_log;
  vector<lower=0>[n_obs] y_latent;
}

transformed parameters {
  vector[n_obs] log_y_pred = log(a) + b * log(x);
}

model {
  y_latent ~ lognormal(log_y_pred, sigma_log);
  y_obs ~ normal(y_latent, sigma_lin);
}
