data {
  int<lower=0> n_obs;
  vector<lower=0>[n_obs] x;
  vector[n_obs] y_obs;
}

parameters {
  real a;
  real b;
  real sigma_lin;
  real sigma_log;
  vector[n_obs] log_y_latent;
}

transformed parameters {
  vector[n_obs] log_y_pred = log(a) + b * log(x);
}

model {
  log_y_latent ~ normal(log_y_pred, sigma_log);
  y_obs ~ normal(exp(log_y_latent), sigma_lin);
}
