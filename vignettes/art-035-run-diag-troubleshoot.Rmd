---
title: "About warnings during a model run"
date: "2023-05-17"
vignette: >
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteIndexEntry{About warnings during a model run}
output:
  rmarkdown::html_vignette
---



This document focuses on running MCMC sampling after having defined a model, and on troubleshooting sampling issues.

Let's prepare our session:


```r
library(ambience)
library(tidyverse)
```

## About Stan's warnings

Stan is very good at spotting problems during MCMC sampling and at warning you about it. It is a great thing! In [the words of Stan's developers](https://mc-stan.org/misc/warnings.html#i-got-no-warnings-when-fitting-the-same-model-in-jagswinbugs):

> You might not be used to seeing so many warnings from other software you use, but that *does not mean* that Stan has more problems than that other software. The Stan Development Team places a high priority on notifying users about any issue that could potentially be important and Stan will always tell you about problems it encounters instead of hiding them from you.

The Stan development team has put together [a very informative webpage](https://mc-stan.org/misc/warnings.html "Runtime warnings and convergence problems") containing explanations about Stan's warnings, and how to address them. It is strongly recommended to read this page for the best explanations, but you will find below is a quick overview of the most common warnings.

## Divergent transitions after warmup

([Stan doc](https://mc-stan.org/misc/warnings.html#divergent-transitions-after-warmup))

Divergences are an indication that the MCMC sampling encountered issues that might make the obtained posteriors unreliable. You should not ignore this warning, and try to address the underlying issue instead. In the words of Stan's developers:

> Even a small number of divergences after warmup cannot be safely ignored if completely reliable inference is desired. But if you get only few divergences and you get good Rhat and ESS values, the resulting posterior is often good enough to move forward.

Here is an example with `run_mcmc()` where we get divergent transitions:


```r
f <- run_mcmc(gryphons_model, seed = 42)

## Warning message (among others):
## 1: There were 14 divergent transitions after warmup. See
## https://mc-stan.org/misc/warnings.html#divergent-transitions-after-warmup
## to find out why this is a problem and how to eliminate them. 
```

Sometimes this might be solved by adjusting the `adapt_delta` setting of Stan:


```r
f2 <- run_mcmc(gryphons_model, seed = 42, control = list(adapt_delta = 0.9))

## Warning messages (among others):
## 1: There were 2 divergent transitions after warmup. See
## https://mc-stan.org/misc/warnings.html#divergent-transitions-after-warmup
## to find out why this is a problem and how to eliminate them. 
```

We have less divergent transitions, which is encouraging. The `adapt_delta` value must be between 0 and 1, and the default is 0.8. Increasing its value to for example 0.9, 0.95, or 0.99 can sometimes help with divergent transitions, at the price of slightly increased runtime. Of course, it is better to have reliable posteriors from a slightly longer MCMC run rather than a fast MCMC run with unreliable posteriors!

Let's try to increase it again:


```r
f3 <- run_mcmc(gryphons_model, seed = 42, control = list(adapt_delta = 0.99))

## No more warnings about divergent transitions
```

If divergences persist even with increased `adapt_delta`, it might indicate deeper problems with the model itself: it might be that the model structure is not adequate for the dataset being modelled and different covariates/random effects might be needed, or it might be that more informative priors are needed.

## R-hat too high

([Stan doc](https://mc-stan.org/misc/warnings.html#r-hat))

R-hat is a convergence diagnostic that tends to 1 when chains converge/mix well. R-hat values larger than 1 indicate problem with the mixing of the chains. In the words of Stan's developers:

> We recommend running at least four chains by default and in general only fully trust the sample if R-hat is less than 1.01. In early workflow, R-hat below 1.1 is often sufficient.

> High R-hat means that the chains have not mixed well and so there is not a good reason to think of them as them being fully representative of the posterior.

High R-hat can occur alongside divergences, in which case solving the divergence issue can sometimes also solve the R-hat problem.

Large R-hat values can also occur when posteriors have several modes. In this case, using more informative priors might help.

## Bulk and tail ESS

([Stan doc](https://mc-stan.org/misc/warnings.html#bulk-and-tail-ess))

Effective sample size (ESS) is an indicator of how much information the Markov chains contain (in terms of how many truly independent draws from the posteriors would contain the same information as the Markov chains).

The higher the ESS, the more precise the estimates from the posteriors such as the mean and quantile estimates. In the words of Stan's developers:

> Overall bulk-ESS estimates the sampling efficiency for location summaries such as mean and median. Often smaller ESS would be sufficient for the desired estimation accuracy, but the estimation of ESS and convergence diagnostics themselves require higher ESS.

> Tail-ESS computes the minimum of the effective sample sizes (ESS) of the 5% and 95% quantiles. Tail-ESS can help diagnose problems due to different scales of the chains and slow mixing in the tails. 

Sometimes solving issues about low bulk ESS and low tail ESS is as simple as running the chains for longer, if no other warnings were present. Often divergences or large R-hat values are also present, and those must be dealt with first.

## Maximum treedepth

([Stan doc](https://mc-stan.org/misc/warnings.html#maximum-treedepth))

In the words of Stan's developers:

> Warnings about hitting the maximum treedepth are not as serious as other warnings. While divergent transitions, high R-hat and low ESS are a *validity* concern, hitting the maximum treedepth is an *efficiency* concern.

> If this is the only warning you are getting and your ESS and R-hat diagnostics are good, it is likely safe to ignore this warning, but finding the root cause could result in a more efficient model.

## BFMI low

([Stan doc](https://mc-stan.org/misc/warnings.html#bfmi-low))

e-BFMI is the estimated Bayesian Fraction of Missing Information. This one is a bit mysterious to me, and the [explanation from Stan's developers](https://mc-stan.org/misc/warnings.html#bfmi-low) refers to some arxiv articles.

I found the [following post](https://discourse.mc-stan.org/t/how-can-i-solve-bfmi-low-problem/3018/18) from Michael Betancourt on Stan's discourse server:

> The E-BFMI (really should be E-FMI) quantifies how well the momentum resampling in HMC works. Low values indicate that the sampler is not able to explore all of the relevant energy sets fast enough which *may* manifest as bias in the exploration of the parameter space.

> If you still see E-FMI values lower than 0.2 then you’ll have to consider reparameterizations/new priors. In particular, in many cases low E-FMI seems to indicate really poor identifiability of the likelihood which will require careful prior choice to ensure a well-behaved joint model.


<nav aria-label="Page navigation">
 <ul class="pagination justify-content-end">
  <li class="page-item"><a class="page-link" href="art-030-priors.html">Previous: Choosing and setting priors</a></li>
  <li class="page-item"><a class="page-link" href="art-040-posterior-predictions.html">Next: Posterior prediction basics</a></li>
 </ul>
</nav>
