---
title: "Issues when estimating measurement uncertainty"
date: "`r Sys.Date()`"
vignette: >
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteIndexEntry{Issues when estimating measurement uncertainty}
output:
  rmarkdown::html_vignette
---

```{r include = FALSE}
knitr::opts_chunk$set(fig.align = "center",
                      fig.path = "figures/art-100-",
                      fig.width = 7, fig.height = 5,
                      dev = "jpeg")
options(scipen = 6, width = 85)
options(mc.cores = parallel::detectCores())
set.seed(4)
```

**Important note on 2022-03-26: It looks like the allometric model is actually working quite nicely even with those datasets. This vignette could be altogether dropped, or I could spend more time to try and generate some problematic dataset.**

**Work in progress**

Note: add discussion about having separate technical and biological replicates, to allow for the distinction between sigma_lin and sigma_log more easily.

Could a syntax like `y_latent ~ ind_id` be used to inform the model that some rows are purely technical replicates? Or can this case be covered with an appropriate random effect structure?

In this document, we explore how we can estimate measurement uncertainty, in which situations it becomes difficult, and how to address those difficulties.

```{r message = FALSE}
library(ambience)
library(tidyverse)
```

## Estimating the measurement variability

(Previous example is from the "Modelling an allometric relationship" vignette.)

The previous example used data for which the measurement variability $\sigma_{lin}$ was quite large, which means that some observed $y$ values were likely to be negative.

However, it might be that the measurement variability is more reduced. Let's simulate new data with similar parameters, except for a smaller $\sigma_{lin}$:

```{r }
set.seed(4)
n <- 25
a <- c(male = 7, female = 8)
b <- c(male = 0.7, female = 0.5)
s_log <- 0.3
s_lin <- 1 # Different value of measurement uncertainty (linear scale)
bm <- runif(2 * n, 0, 10)
low_slin <- tibble(mass = bm,
               sex = c(rep("male", n), rep("female", n)),
               log_excr_pred = rnorm(2 * n,
                                     log(a[sex]) + b[sex] * log(mass),
                                     s_log),
               excr_obs = rnorm(2 * n,
                            exp(log_excr_pred),
                            s_lin))
ggplot(low_slin, aes(x = mass, y = excr_obs, col = sex)) +
  geom_point()
```

Now let's run an allometric model:

```{r message = FALSE, warning = FALSE, results = "hide"}
m <- allom(excr_obs ~ mass, data = low_slin,
           a + b ~ sex)
f <- run_mcmc(m, seed = 42) # `seed` argument used for reproducibility
```

If you run this command, you'll see that Stan produces several warnings about issues with the sampling:

- divergent transitions
- low Bayesian Fraction of Missing Information
- large R-hat value
- effective samples size too low.

Those are serious warnings and should not be ignored (see https://mc-stan.org/misc/warnings.html for a guide to Stan warnings). Let's have a quick look at the traces:

```{r fig.width = 7}
traceplot(f)
```

As we can see from the warnings and from the traces, the posterior for $\sigma_{lin}$ is hard to sample. This is because $\sigma_{lin}$ is often hard to estimate when the measured values for $y$ are all positive: in this case, the variability due to measurement tends to be absorbed in the estimate of $\sigma_{log}$.

One way to solve some of the issues arising during the MCMC run above is to let Stan use smaller steps. Let's run a model where we adjust the `adapt_delta` Stan run setting using the `control()` argument:

```{r message = FALSE, warning = FALSE, results = "hide"}
f2 <- run_mcmc(m, control = list(adapt_delta = 0.99), seed = 42)
traceplot(f2)
```

In this case, the situation is not improved: the issue is not with the sampling itself but rather with the model specification.

In this case, we can decide to drop the $\sigma_{lin}$ part of the model and just let all the variability be absorbed by $\sigma_{log}$. This might not be ideal, but it can still be a good approximation of the mechanism generating the data if the measurement error is small (or if it is itself on the log scale, i.e. a multiplicative measurement error). Let's run a model where we do not incorporate a $\sigma_{lin}$ effect by specifying `s_lin = FALSE`:

```{r message = FALSE, warning = FALSE, results = "hide"}
m2 <- allom(excr_obs ~ mass, data = low_slin,
            a + b ~ sex, s_lin = FALSE)
f2 <- run_mcmc(m2, seed = 42)
traceplot(f2)
```

The traces look good, but as we can see the estimated values for $\sigma_{log}$ are slightly higher than in the previous model. Remember that the true value was 0.3:

```{r }
summary(f2)
```

As far as I understand, this positive bias is due to the mesurement uncertainty being absorbed in the biological variability in this model.

**TODO** A vignette with simulations showing if there is a systematic bias in estimating the allometric parameters when $\sigma_{lin}$ is not included?

Given that not including measurement uncertainty in the model is likely to result in some model bias for the allometric parameters, it might be preferable to keep it and to try to improve the MCMC run, or possibly by using repeated measures if more data can be acquired.

TODO Test datasets where there are no negative observations, but repeated measures for the same individuals. We don't currently have a way to model a given sample being measured several times with ambience, but could replicating the whole measurement protocol for some individuals help to distangle s_log and s_lin thanks to random effects?

<nav aria-label="Page navigation">
 <ul class="pagination justify-content-end">
  <li class="page-item"><a class="page-link" href="art-060-compare-treatments.html">Previous: Comparing treatments using posterior predictions</a></li>
  <li class="page-item"><a class="page-link" href="art-200-case-study-aquatic.html">Next: Analysis of excretion rates of aquatic animals</a></li>
 </ul>
</nav>
